<?php
/**
 * The plugin bootstrap file.
 *
 * @package GGM_Call_To_Action
 *
 * Plugin Name: GGM Call To Action
 * Plugin URI: #
 * Description: A plugin that creates call to action template for your WordPress site.
 * Version: 1.0.0
 * Author: Gerard Kristoffer Sabado
 * Author URI: #
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: ggm-call-to-action
 */

/*
Copyright 2019 Gerard Kristoffer Sabado (email : gkcsabado@gmail.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Define.
define( 'GGM_CALL_TO_ACTION_NAME', 'GGM Call To Action' );
define( 'GGM_CALL_TO_ACTION_SLUG', 'ggm-call-to-action' );
define( 'GGM_CALL_TO_ACTION_VERSION', '1.0.0' );
define( 'GGM_CALL_TO_ACTION_BASENAME', basename( dirname( __FILE__ ) ) );
define( 'GGM_CALL_TO_ACTION_DIR', rtrim( plugin_dir_path( __FILE__ ), '/' ) );
define( 'GGM_CALL_TO_ACTION_URL', rtrim( plugin_dir_url( __FILE__ ), '/' ) );
define( 'GGM_CALL_TO_ACTION_POST_TYPE_CTA', 'ggm_cta' );


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ggm-call-to-action-activator.php
 */
function activate_ggm_call_to_action() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ggm-call-to-action-activator.php';
	GGM_Call_To_Action_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ggm-call-to-action-deactivator.php
 */
function deactivate_ggm_call_to_action() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ggm-call-to-action-deactivator.php';
	GGM_Call_To_Action_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ggm_call_to_action' );
register_deactivation_hook( __FILE__, 'deactivate_ggm_call_to_action' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ggm-call-to-action.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ggm_call_to_action() {

	$plugin = new GGM_Call_To_Action();
	$plugin->run();

}
run_ggm_call_to_action();
