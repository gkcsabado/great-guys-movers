<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    GGM_Call_To_Action
 * @subpackage GGM_Call_To_Action/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    GGM_Call_To_Action
 * @subpackage GGM_Call_To_Action/public
 * @author     Gerard Kristoffer Sabado
 */
class GGM_Call_To_Action_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of the plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ggm-call-to-action-public' . $min . '.css', array(), $this->version, 'all' );

	}

	/**
	 * Register custom post type.
	 *
	 * @since    1.0.0
	 */
	public function custom_post_types() {

		// Register Call To Action Post Type.
		$labels = array(
			'name'               => _x( 'GGM Call To Actions', 'post type general name', 'ggm-call-to-action' ),
			'singular_name'      => _x( 'GGM Call To Action', 'post type singular name', 'ggm-call-to-action' ),
			'menu_name'          => _x( 'GGM Call To Action', 'admin menu', 'ggm-call-to-action' ),
			'name_admin_bar'     => _x( 'GGM Call To Action', 'add new on admin bar', 'ggm-call-to-action' ),
			'add_new'            => _x( 'Add New', 'ggm_cta', 'ggm-call-to-action' ),
			'add_new_item'       => __( 'Add New Call To Action', 'ggm-call-to-action' ),
			'new_item'           => __( 'New Call To Action', 'ggm-call-to-action' ),
			'edit_item'          => __( 'Edit Call To Action', 'ggm-call-to-action' ),
			'view_item'          => __( 'View Call To Action', 'ggm-call-to-action' ),
			'all_items'          => __( 'All Call To Actions', 'ggm-call-to-action' ),
			'search_items'       => __( 'Search Call To Actions', 'ggm-call-to-action' ),
			'parent_item_colon'  => __( 'Parent Call To Actions:', 'ggm-call-to-action' ),
			'not_found'          => __( 'No call to actions found.', 'ggm-call-to-action' ),
			'not_found_in_trash' => __( 'No call to actions found in Trash.', 'ggm-call-to-action' ),
			);

		$args = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'show_in_rest'		 => true,
			'query_var'          => false,
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_icon'          => 'dashicons-megaphone',
			'supports'           => array( 'title', 'editor' ),
			);

		register_post_type( GGM_CALL_TO_ACTION_POST_TYPE_CTA, $args );

	}

	/**
	 * Callback function of shortcode `ggm_cta`.
	 *
	 * @since 1.0.0
	 *
	 * @param array $atts Attributes.
	 * @return string Shortcode output.
	 */
	public function shortcode_cb_ggm_cta( $atts ) {

		$atts = shortcode_atts( array(
			'id' => '',
		), $atts, 'WLS' );

		$atts['id'] = absint( $atts['id'] );

		$is_valid_cta = $this->check_if_valid_cta( $atts );

		if ( ! $is_valid_cta ) {
			return __( 'Call To Action not found.', 'ggm-call-to-action' );
		}

		// Fetch default template.
		$cta_theme = $this->get_default_cta_theme();
		$cta_theme = apply_filters( 'ggm_call_to_action_filter_cta_theme', $cta_theme, $atts['id'] );

		// Bail if template is empty.
		if ( empty( $cta_theme ) ) {
			return;
		}

		ob_start();

		$content_display = $this->replace_cta_placeholders( $cta_theme, $atts );

		echo $content_display;

		$output = ob_get_contents();
		ob_end_clean();
		return $output;

	}

	/**
	 * Return default CTA template.
	 *
	 * @since 1.0.0
	 *
	 * @param string $template CTA template.
	 * @param array  $args     Arguments.
	 * @return string Modified CTA template.
	 */
	function replace_cta_placeholders( $template, $args ) {

		$post_id = $args['id'];
		$post_obj = get_post( $post_id );

		if ( null === $post_obj ) {
			return $template;
		}

		// Preparing search replace array.
		$search_array = array();
		$replace_array = array();


		// Description.
		$search_array[] = '{{description}}';
		$description_content = '';
		if ( ! empty( $post_obj->post_content ) ) {
			$description_content = apply_filters( 'the_content', $post_obj->post_content );
		}
		$replace_array[] = $description_content;

		$output = '';
		$output = str_replace( $search_array, $replace_array, $template );

		return $output;

	}

	/**
	 * Return default CTA template.
	 *
	 * @since 1.0.0
	 */
	function get_default_cta_theme() {

		$output = '';

		$output .= '{{description}}';

		$output = apply_filters( 'ggm_call_to_action_filter_default_cta_theme', $output );

		return $output;

	}

	/**
	 * Add extra custom class in CTA wrap.
	 *
	 * @since 1.0.0
	 *
	 * @param array $classes The array of classes.
	 * @param int   $post_id The Post ID.
	 * @return array  Modified array of classes.
	 */
	function add_extra_custom_class( $classes, $post_id ) {

		$cta_theme = get_post_meta( $post_id, '_cta_theme', true );

		if ( ! empty( $cta_theme ) && 'no-style' !== $cta_theme ) {

			$classes[] = 'ggm-cta-template-' . esc_attr( $cta_theme );

		}

		return $classes;

	}

	/**
	 * Check if given id is valid CTA.
	 *
	 * @since 1.0.0
	 *
	 * @param array $args Arguments.
	 * @return bool True if CTA is valid, otherwise false.
	 */
	private function check_if_valid_cta( $args ) {

		$output = false;

		if ( isset( $args['id'] ) && intval( $args['id'] ) > 0  ) {

			$cta = get_post( intval( $args['id'] ) );

			if ( ! empty( $cta ) && 'publish' === $cta->post_status && GGM_CALL_TO_ACTION_POST_TYPE_CTA === $cta->post_type ) {
				$output = true;
			}
		}
		return $output;

	}
}
