=== GGM Call To Action ===
Contributors: Gerard Kristoffer Sabado
Donate link: #
Tags: Call To Action, Calls To Action, CTA, CTA Button, shortcode, widget, call outs, call out, message box, conversion box, button, shortcode button
Requires at least: 4.8
Tested up to: 5.3
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A plugin that creates call to action template for your WordPress site.

== Description ==

<h3>GGM Call To Action</h3>
GGM Call To Action creates call to action for your WordPress site. This plugin gives you markup for Call To Action with simple styling. You can easily customize by adding custom class. Extension of CTA depends on your imagination.

== Changelog ==

= 1.0.0 =
* Initial release

== Upgrade Notice ==
GGM Call To Action
