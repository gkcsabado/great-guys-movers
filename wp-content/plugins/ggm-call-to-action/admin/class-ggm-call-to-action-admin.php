<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0
 *
 * @package    GGM_Call_To_Action
 * @subpackage GGM_Call_To_Action/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    GGM_Call_To_Action
 * @subpackage GGM_Call_To_Action/admin
 * @author     Gerard Kristoffer Sabado
 */
class GGM_Call_To_Action_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string $plugin_name       The name of this plugin.
	 * @param      string $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since 1.0.0
	 */
	public function enqueue_styles() {

		$screen = get_current_screen();
		if ( GGM_CALL_TO_ACTION_POST_TYPE_CTA === $screen->id ) {
			$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ggm-call-to-action-admin' . $min . '.css', array(), $this->version, 'all' );
		}

	}

	/**
	 * Manage column head in admin listing.
	 *
	 * @since 1.0.0
	 *
	 * @param array $columns An array of column names.
	 * @return array Modified array of column names.
	 */
	function usage_column_head( $columns ) {

		$new_columns['cb']     = '<input type="checkbox" />';
		$new_columns['title']  = $columns['title'];
		$new_columns['id']     = _x( 'ID', 'column name', 'ggm-call-to-action' );
		$new_columns['usage']  = __( 'Usage', 'ggm-call-to-action' );
		$new_columns['date']   = $columns['date'];
		return $new_columns;

	}

	/**
	 * Content for extra column in admin listing.
	 *
	 * @since    1.0.0
	 *
	 * @param array $column_name The name of the column to display.
	 * @param array $post_id The current post ID.
	 */
	function usage_column_content( $column_name, $post_id ) {

		switch ( $column_name ) {
			case 'id':
				echo $post_id;
			break;

			case 'usage':
				echo '<code>[ggm_cta id="' . $post_id . '"]</code>';
			break;

			default:
			break;
		}

	}

	/**
	 * Hide publishing actions in edit page.
	 *
	 * @since    1.0.0
	 */
	public function hide_publishing_actions() {
		global $post;
		if ( GGM_CALL_TO_ACTION_POST_TYPE_CTA !== $post->post_type ) {
			return;
		}
	?>
    <style type="text/css">
    #misc-publishing-actions,#minor-publishing-actions{
      display:none;
    }
    </style>
    <?php
	return;
	}

	/**
	 * Customize post row actions.
	 *
	 * @since 1.0.0
	 *
	 * @param array   $actions An array of row action links.
	 * @param WP_Post $post The post object.
	 */
	function customize_row_actions( $actions, $post ) {

		if ( GGM_CALL_TO_ACTION_POST_TYPE_CTA === $post->post_type ) {

			unset( $actions['inline hide-if-no-js'] );

		}

		return $actions;

	}

	/**
	 * Add meta boxes.
	 *
	 * @since 1.0.0
	 *
	 * @param string $post_type Post type.
	 */
	function add_cta_meta_boxes( $post_type ) {

		// Bail if not our post type.
		if ( GGM_CALL_TO_ACTION_POST_TYPE_CTA !== $post_type ) {
			return;
		}

		$screens = array( GGM_CALL_TO_ACTION_POST_TYPE_CTA );

		foreach ( $screens as $screen ) {
			add_meta_box(
				'ggm_call_to_action_usage_content_id',
				__( 'Usage', 'ggm-call-to-action' ),
				array( $this, 'usage_meta_box_callback' ),
				$screen,
				'side'
			);

		}

	}

	/**
	 * Callback for usage metabox.
	 *
	 * @since    1.0.0
	 *
	 * @param WP_Post $post Post object.
	 */
	function usage_meta_box_callback( $post ) {

	?>
    <h4><?php _e( 'Shortcode', 'ggm-call-to-action' ); ?></h4>
    <p><?php _e( 'Copy and paste this shortcode directly into any WordPress post or page.', 'ggm-call-to-action' ); ?></p>
    <textarea class="large-text code" readonly="readonly"><?php echo '[ggm_cta id="'.$post->ID.'"]'; ?></textarea>

    <h4><?php _e( 'Template Include', 'ggm-call-to-action' ); ?></h4>
    <p><?php _e( 'Copy and paste this code into a template file to include the slider within your theme.', 'ggm-call-to-action' ); ?></p>
    <textarea class="large-text code" readonly="readonly">&lt;?php echo do_shortcode("[ggm_cta id='<?php echo $post->ID; ?>']"); ?&gt;</textarea>
    <?php

	}
}
