<?php
/**
 * Fired during plugin deactivation
 *
 * @since      1.0.0
 *
 * @package    GGM_Call_To_Action
 * @subpackage GGM_Call_To_Action/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    GGM_Call_To_Action
 * @subpackage GGM_Call_To_Action/includes
 * @author     Gerard Kristoffer Sabado
 */
class GGM_Call_To_Action_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}
}
