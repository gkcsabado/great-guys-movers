//globals - greatguyslongdistancemovers
if(!getCookie('mpf_uid')){
	var mpf_uid = uniqueID();
	setCookie('mpf_uid',uniqueID(),7);
} else {
	var mpf_uid = getCookie('mpf_uid');
}

var page_url = window.location.pathname;
var form_values;
var all_done;
var recent_moves;
var toastr_done = new Object();
var need_consent = 0;

jQuery(document).ready(function($) {
	if(getCookie('quote_success')){
		hideQuoteButtons();
	}	
	
	gtag('config', 'GA_TRACKING_ID', {
	  'user_id': mpf_uid
	});

	
	// setTimeout(function(){
		// toastr["success"](recent_moves[2].first_name + " just got a quote for movers from "+ recent_moves[2].region + " to " +  recent_moves[2].region2 + "!");
	// }, Math.floor(Math.random() * 12) + 4);	

	//phoneNumber Inputmask
	$("#phone_number").mask("(999) 999-9999");

	//bootstrap switch - Yes/NO Running
	$("[name='auto_running']").bootstrapSwitch.defaults.size = 'large';
	$("[name='auto_running']").bootstrapSwitch.defaults.onColor = 'success';
	$("[name='auto_running']").bootstrapSwitch.defaults.offColor = 'danger';
	$("[name='auto_running']").bootstrapSwitch.defaults.labelText = 'Is Vehicle Running?';
	$("[name='auto_running']").bootstrapSwitch.defaults.onText = 'YES';
	$("[name='auto_running']").bootstrapSwitch.defaults.offText = 'NO';
	$("[name='auto_running']").bootstrapSwitch.defaults.state = true;		
	$("[name='auto_running']").bootstrapSwitch();
	$("#auto_running").val(1);

	recent_moves();
	
	// // Show/Hide "other" make & model
	// //disable make/model ajax 1/28/18
	// $("#make").change(function() {
	// $("#make").change(function() {
		// if ($("#make").val() == 22) {
			// $('#other_make').show();
			// $('#model_original').hide();
			// $('#other_model').show();
		// } else {
			// // if the user switches back and selects somethign else, show original form
			// $('#other_make').hide();
			// $('#model_original').show();
			// $('#other_model').hide();
		// }
	// });

	// get height of fieldset 
	var formHeight = $("fieldset").height();
	$(".formHeight").css("min-height", formHeight);

	// zebra_datepicker 
	//setting default date for quote
	Date.prototype.addDays = function(days) {
	  var dat = new Date(this.valueOf());
	  dat.setDate(dat.getDate() + days);
	  return dat;
	}
	var dat = new Date();	
	var start_date_15 = dat.addDays(15);
	$('#move_date').Zebra_DatePicker({
		direction: [1, 180],
		format: 'm/d/Y',
		start_date: start_date_15,
	});
	$( "#move_date" ).attr( "value", (start_date_15.getMonth() + 1) + '/' + start_date_15.getDate() + '/' +  start_date_15.getFullYear() );



	//jQuery time
	var current_fs, next_fs, previous_fs; //fieldsets
	var left, opacity, scale; //fieldset properties which we will animate
	var animating; //flag to prevent quick multi-click glitches

	//consent agree
	$("#button_consent_agree").click(function() {
		console.log("consent button agreed");
		$("#consent_agree").val("1");
	});		
	
	
	//next & previous step : http://www.jqueryscript.net/form/Creating-A-Modern-Multi-Step-Form-with-jQuery-CSS3.html
	$(".next").click(function() {
		//create json of form inputs
		var form_serialize = ( $( "#multi-part-form" ).serializeArray() );
			form_values = {};
			$(form_serialize).each(function(i, field){
			if(field.name == 'phone_number'){
				//strip formatting from phone number
				form_values[field.name] = field.value.replace(/\D/g,'');
			} else {
				form_values[field.name] = field.value;
			}
		});		
		
		var isValid = true;
		//remove up arrow
		$("#toTop").remove();
		if ($(this).is('#start_quote')) {
			gtag('event', 'form_start', {'event_category': 'form_steps'});		////analytics
		} else if ($(this).is('#button1')) { //check from and to zip in first step
			form_from = $('#autocomplete').val();
			gtag('event', 'form_move_from', {'event_category': 'form_steps', 'event_label':form_from}); ////analytics
			isValid = $('#postal_code').valid() ? isValid : false;		
		} else if ($(this).is('#button2')) { //check move date and size in second step
			form_to = $('#autocomplete2').val();			
			gtag('event', 'form_move_to', {'event_category': 'form_steps', 'event_label':form_to}); ////analytics
			if ($(this).hasClass("disabled")) { //disabled if zip-lookup is searching
				isValid = false;
				gtag('event', 'form_move_to_error', {'event_category': 'form_steps', 'event_label':form_to}); ////analytics
			} else {	
				isValid = $('#postal_code2').valid() ? isValid : false;
				gtag('event', 'form_move_to_confirmed', {'event_category': 'form_steps'}); ////analytics
			}	
		} else if ($(this).is('#button_move_date')) { //check move date
			isValid = $('#move_date').valid() ? isValid : false;
			gtag('event', 'form_move_date', {'event_category': 'form_steps', 'event_label':form_values['move_date']});		////analytics
		} else if ($(this).is('#button_move_size')) { //check move date
			isValid = $('#move_size').valid() ? isValid : false;
			gtag('event', 'form_move_size', {'event_category': 'form_steps', 'event_label':form_values['move_size']});		////analytics
		} else if ($(this).is('#move_details_confirmed')) { //check move date
			gtag('event', 'form_move_details_confirmed', {'event_category': 'form_steps'});		////analytics
		} else if ($(this).is('#no_not_right')) { //check move date
			gtag('event', 'form_move_details_wrong', {'event_category': 'form_steps'});		////analytics
		} else if ($(this).hasClass('move_from_type')){
			isValid = $('#move_from_type').valid() ? isValid : false;
			gtag('event', 'form_move_from_type', {'event_category': 'form_steps'});		////analytics
		} else if ($(this).is('#button_user_name')) { //check move date and size in second step
			isValid = $('#first_name').valid() ? isValid : false;
			//isValid = $('#last_name').valid() ? isValid : false;
			//toastr for empty-name/num/email - only 1 of these
			if($('#last_name').valid()){
				isValid = isValid;				
				gtag('event', 'form_name', {'event_category': 'form_steps'});		////analytics
			} else {
				isValid = false;
				gtag('event', 'form_name_error', {'event_category': 'form_steps'});		////analytics
				setTimeout(function(){
					if(!toastr_done.three){
						toastr["success"](recent_moves[3].first_name + " just got a quote for movers from "+ recent_moves[3].region + " to " +  recent_moves[3].region2 + "!");
						toastr_done.three = true;
					}	
				}, 3500);
			}			
		}  else if ($(this).is('#button_email')) { //check move date and size in second step
			//isValid = $('#email').valid() ? isValid : false;
			//toastr for empty-name/num/email - only 1 of these
			if($('#email').valid()){
				isValid = isValid;
				gtag('event', 'form_email', {'event_category': 'form_steps'});		////analytics
			} else {
				isValid = false;
				gtag('event', 'form_email_error', {'event_category': 'form_steps'});		////analytics
				setTimeout(function(){
					if(!toastr_done.three){
						toastr["success"](recent_moves[3].first_name + " just got a quote for movers from "+ recent_moves[3].region + " to " +  recent_moves[3].region2 + "!");
						toastr_done.three = true;
					}	
				}, 3500);
			}			
		} else if ($(this).is('#button_phone')) { //check move date and size in second step
			//isValid = $('#phone_number').valid() ? isValid : false;
			//toastr for empty-name/num/email - only 1 of these
			if($('#phone_number').valid()){
				isValid = isValid;
				gtag('event', 'form_phone', {'event_category': 'form_steps'});		////analytics
				gtag('event', 'form_submit', {'event_category': 'form_steps'});		////analytics
			} else {
				isValid = false;
				gtag('event', 'form_phone_error', {'event_category': 'form_steps'});		////analytics
				setTimeout(function(){
					if(!toastr_done.three){
						toastr["success"](recent_moves[3].first_name + " just got a quote for movers from "+ recent_moves[3].region + " to " +  recent_moves[3].region2 + "!");
						toastr_done.three = true;
					}	
				}, 3500);
			}
		}
		
		if (animating || !isValid) {
			return false;
		}
		animating = true;
		current_fs = $(this).parents('fieldset');
		
		
		
		
		if ( form_values['auto_make'] && form_values['auto_running'] == 'on') {
			form_values['auto_running'] = 1;
		} else if ( form_values['auto_running'] && form_values['auto_running'] == 'off') {
			form_values['auto_running'] = 0;
		}
		form_values['page_url'] = page_url;
		if(form_values['region'] && form_values['region2'] && form_values['region'] != form_values['region2']){
			form_values['interstate'] = 1;
		}
		if(form_values['country'] && form_values['country'] && form_values['country'] != form_values['country']){
			form_values['international'] = 1;
		}	
		if(form_values['auto_year'] && form_values['auto_model'] ){
			// form_values['auto_running'] = 1;
		}

		if ($(".flagged").length == 0 && $(".allConfirmed").length == 0 && form_values['postal_code'] ){
			saveSteps(form_values,mpf_uid); //only save if we've got good data
		}
		// if ($(".confirmation_success").length > 0){ //confirmation attempt
			// next_fs = $( "#confirmation_fieldset" );			
		// } 
		
		if($(this).is('#button_confirmation') ) {
			//if on the final confirmation step - go to additional
			next_fs = $("#additional");
		}  else if($(this).is('#button_car') && form_values['move_size'] != 'Auto' ) {
			//if auto, no need to run checks - send straight to confirmation
			$("#confirmed").addClass("allConfirmed");
			all_done = true;
			setCookie('quote_success', 1,7);
			next_fs = $("#confirmed");
			//iff addtl. auto filled out - track analytics
			if(form_values['auto_make']){
				gtag('event', 'form_submit_auto', {'event_category': 'form_steps'});		////analytics
			}
		} else if ($(".flagged").length > 0){ //cycle through flagged inputs
			next_fs = $( ".flagged:first" ).parents('fieldset'); //find first and make it next up
			$( ".flagged:first" ).removeClass('flagged'); // after marking as next, remove flagged tag
			$("#confirmation_fieldset").addClass("validation_waiting"); //need to send to validation next **what if multiple flagged???		
		}  else {
			if ($(".validation_waiting").length > 0){ 
				//if validation tag thrown, send to confirmation fieldset
				next_fs = $("#confirmation_fieldset");
				gtag('event', 'form_error', {'event_category': 'form_steps'});		////analytics
			} else if ($(".allConfirmed").length > 0) {
				gtag('event', 'form_success', {'event_category': 'form_steps'});		////analytics
				//all initial fields are confirmed, run check for interstate
				if(form_values['interstate'] && form_values['move_size'] != 'Auto' && form_values['move_size'] != 'Art' && form_values['move_size'] != 'Pool Table' && form_values['move_size'] != 'Piano'){	//all good - send to auto or not?					
					//need to make sure it's not an auto-only, in which case send to final confirmation
					next_fs = $("#auto");
					$("#confirmation_fieldset").addClass("validation_waiting");
					$(".allConfirmed").removeClass();
					gtag('event', 'form_auto_shown', {'event_category': 'form_steps'});		////analytics
				} else {
					next_fs = $("#confirmed");
					all_done = true;
					setCookie('quote_success', 1,7);
				}
			} else if ($(this).is('#button_move_size') && form_values['move_size'] == 'Auto') {
				//Need to additionally check button type because move_size will be auto for rest of form. we only want this happening on this specific step
				//Someone selected only Auto - send to auto before contact info
				next_fs = $("#auto");
			} else if ($(this).is('#button_car') && form_values['move_size'] == 'Auto' ) {
				//The normal "next" from auto is nothing since its at the end, instead send to confirmation.
				next_fs = $("#name");
			} else {
				//proceed as normal (initial steps) - find next fieldset
				next_fs = $(this).parents('fieldset').next();
			}
		}
		//show the next fieldset
		
		//hide the current fieldset with style
		current_fs.animate({
			opacity: 0
		}, {
			step: function(now, fx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale current_fs down to 80%
				scale = 1 - (1 - now) * 0.01;
				//2. bring next_fs from the right(50%)
				left = (now * 50) + "%";
				//3. increase opacity of next_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
					'transform': 'scale(' + scale + ')'
				});
				next_fs.css({
					'left': left,
					'opacity': opacity
				});
			},
			duration: 250,
			complete: function() {
				current_fs.hide();
				animating = false;
				next_fs.show();
				// var formHeight = next_fs.height();
				// $(".formHeight").css("min-height", formHeight);
			},
			//this comes from the custom easing plugin
			easing: 'linear'
		});
	});

	$(".previous").click(function() {
		if (animating) return false;
		animating = true;

		current_fs = $(this).parents('fieldset');
		previous_fs = $(this).parents('fieldset').prev();

		//show the previous fieldset
		//hide the current fieldset with style
		current_fs.animate({
			opacity: 0
		}, {
			step: function(now, mx) {
				//as the opacity of current_fs reduces to 0 - stored in "now"
				//1. scale previous_fs from 80% to 100%
				scale = 0.8 + (1 - now) * 0.2;
				//2. take current_fs to the right(50%) - from 0%
				left = ((1 - now) * 50) + "%";
				//3. increase opacity of previous_fs to 1 as it moves in
				opacity = 1 - now;
				current_fs.css({
					'left': left
				});
				previous_fs.css({
					'transform': 'scale(' + scale + ')',
					'opacity': opacity
				});
			},
			duration: 100,
			complete: function() {
				current_fs.hide();
				animating = false;				
				previous_fs.show();
				var formHeight = next_fs.height();
				$(".formHeight").css("min-height", formHeight);
			},
			//this comes from the custom easing plugin
			easing: 'easeInOutBack'
		});
	});	
	

	//custom validation for us date format
	$.validator.addMethod("usDateFormat",
		function(value, element) {
			var check = false;
			var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
			if (re.test(value)) {
				var adata = value.split('/');
				var mm = parseInt(adata[0], 10);
				var dd = parseInt(adata[1], 10);
				var yyyy = parseInt(adata[2], 10);
				var xdata = new Date(yyyy, mm - 1, dd);
				if ((xdata.getFullYear() == yyyy) && (xdata.getMonth() == mm - 1) && (xdata.getDate() == dd))
					check = true;
				else
					check = false;
			} else
				check = false;
			return this.optional(element) || check;
		},
		"Please enter a date in the format mm/dd/yyyy");	
	
		
	//form validation
	$("#multi-part-form").validate({
		ignore: "", //override default setting to validate hidden inputs
		rules: {
			postal_code: {
				required: true,
				//digits: true,
			},
			postal_code2: {
				required: true,
			},
			move_from_type: {
				required: true,	
			},
			move_size: {
				required: true,	
			},
			first_name: {
				required: true,
			},
			last_name: {
				required: true,
			},
			move_date: {
				required: true,
			},
			year: {
				required: true,
			},
			make: {
				required: true,
			},
			email: {
				required: true,
				email: true
			},
			phone_number: {
				required: true,
				minlength: 10,
			},
		},
		errorPlacement: function(error, element) {
			element.parents('.form-group').append(error)
			//error.insertAfter(".form-group");				
		},
		messages: { //custom messages
			move_from_type: {
				required: "Please select the type of place that best represents your move FROM location.",
				digits: "Only digits allowed",
			},
			first_name: {
				required: "Sorry, we need your name to continue",
			},
			last_name: {
				required: "We need at least need an initial for your last name",
			},
			email: {
				required: "Sorry, we need your email to send you confirmation",
				email: "Please enter a valid email address",
			},
			phone_number: {
				required: "We need your number to have a licensed moving rep call you back.",
				minlength: "Please enter your full 10 digit number",
			},
			move_size: {
				required: "Please select the approximate size that best represents your move.",
			},
			postal_code: {
				required: "Please enter AND select a valid address (either zipcode or your full address) from the dropdown options"
			},
			postal_code2: {
				required: "Please enter AND select a valid location (either city & state, zip code, or full address) from the dropdown options"
			},
		}
	});
	

	$(".quote .icon-block").click(function(){   
		var check=$(this).attr('class');  
		var type = $(this).attr('type'); 
		var selectedValue = $(this).attr('opt-value');
		var location = $(this).attr('location');
		
		if($(this).hasClass('IsChecked') && location == 'from'){ 
			$(this).removeClass('IsChecked');
			$('input[name=move_from_type]').val(''); //reset hidden input to null
			//$('#move_from_type').trigger('change'); //trigger change programmatically
		} 
		if(!$(this).hasClass('IsChecked') && location == 'from') {
			$(".quote .icon-block[location='from']").removeClass('IsChecked'); //first remove it from everything else
			$(this).addClass('IsChecked'); //Then add it to the correct checked element
			$('input[name=move_from_type]').val(selectedValue);
			//$('#move_from_type').trigger('change'); //trigger change programmatically
		}
		
		if($(this).hasClass('IsChecked') && location == 'to'){ 
			$(this).removeClass('IsChecked');
			$('input[name=move_to_type]').val(''); //reset hidden input to null
		} 
		if(!$(this).hasClass('IsChecked') && location == 'to') {
			$(".quote .icon-block[location='to']").removeClass('IsChecked'); //first remove it from everything else
			$(this).addClass('IsChecked'); //Then add it to the correct checked element
			$('input[name=move_to_type]').val(selectedValue);
		}		
				 
	});	
	
	//Handle all the Responses
	//your vs another - location types

	$( "#button1" ).click(function() {
		$("#mpf-response-2").html('Great! We\'d love to help you with your move from ' + form_values['city'] + '.');
		setTimeout(function(){
			if(!toastr_done.zero){
				toastr["success"](recent_moves[0].first_name + " just got a quote for movers from "+ recent_moves[0].region + " to " +  recent_moves[0].region2 + "!");
				toastr_done.zero = true;
			}	
		}, 3500);
	});	
	
	$( "#button2" ).click(function() {
		$("#mpf-response-3").html(form_values['city'] + ' to ' + form_values['city2'] + '... not a problem');
	});	
	$( "#button_move_from_type" ).click(function() {
		if(form_values['move_from_type'] == "other"){
			$("#mpf-response-4").html('Not to worry. We handle all types of moves. I\'m sure we can accommodate your unique situation');
		} else {
			$("#mpf-response-4").html('Moving from your ' + form_values['move_from_type'] + '. that\'s easy!');
		}
		//change bedrooms to offices
		if(form_values['move_from_type'] == "business"){
			$("#move_size option").text(function (_, ctx) {
				return ctx.replace("Bedroom", "Office");
			});		
		}
		setTimeout(function(){
			if(!toastr_done.one){
				toastr["success"](recent_moves[1].first_name + " just got a quote for movers from "+ recent_moves[1].region + " to " +  recent_moves[1].region2 + "!");
				toastr_done.one = true;
			}	
		}, 4000);		
	});		
	$( "#button_move_to_type" ).click(function() {
		if(form_values['move_from_type'] == form_values['move_to_type']){
			var your_another = "another";
		} else {
			var your_another = "your";
		}		
		if(form_values['move_from_type'] == "other" || form_values['move_to_type'] == "other"){
			$("#mpf-response-5").html('Not to worry. We handle all types of moves. I\'m sure we can accommodate your unique situation');
		} else {
			$("#mpf-response-5").html('Moving from your ' + form_values['move_from_type'] + ' and to ' + your_another + ' ' + form_values['move_to_type'] + '... no problem!');
		}		
	});
	$("#button_move_size").click(function() {
		if(form_values['move_from_type'] == form_values['move_to_type']){
			var your_another = "another";
		} else {
			var your_another = "your";
		}
		
		if(form_values['move_from_type'] == "business"){
			var bedrooms_offices = form_values['move_size'].replace('Bedroom','Office');
		} else {
			var bedrooms_offices = form_values['move_size'];
		}
		
		if(form_values['move_size'] == "Auto") {
			var confirmation_response = 'So, you\'re moving a car from ' + form_values['city'] + ' to ' + form_values['city2'] + ' on ' + form_values['move_date'] + '.';
		} else if(form_values['move_from_type'] == "other" || form_values['move_to_type'] == "other") {
			//instead of saying " so you're moving 2 bedrooms form your other in city to another other in city on xx/xx/xxxx"
			var confirmation_response = 'So, you\'re moving ' + bedrooms_offices + ' from ' + form_values['city'] + ' to ' + form_values['city2'] + ' on ' + form_values['move_date'] + '.';
		} else {
			var confirmation_response = 'So, you\'re moving ' + bedrooms_offices + ' from your ' + form_values['move_from_type'] + ' in ' + form_values['city'] + ' to ' + your_another + ' ' + form_values['move_to_type'] + ' in ' + form_values['city2'] + ' on ' + form_values['move_date'] + '.';
		}
		
		$("#mpf-response-7").html(confirmation_response);
		
		
	});	
	$("#button_user_name").click(function() {
		var name = form_values['first_name'];
		$("#mpf-response-9").html('Thanks ' + name + '! We\'re almost finished.');
	});	

	
	// //auto_make_models lookup
    // var e = "";
    // $("#auto_year").change(function() {
        // var t = $("#auto_year").find(":selected").val();
        // var n = "";
        // e = getJson(ajaxurl+"?action=car_models&year=" + t);
		// if (e.length == 0) {
			// e = getJson(ajaxurl+"?action=car_models&year=" + '2013');
		// }
        // var r = $("#auto_make");
        // r.empty();  //uncomment if you want to remove the default select 
		// r.append($("<option />").val('').text('Select Make')) // add default 'other' select option
        // $.each(e, function() {
            // if (this.make != n) {
                // r.append($("<option />").val(this.make).text(this.make))
            // }
            // n = this.make
        // });
		// r.append($("<option />").val('22').text('Other')) // add default 'other' select option
        // $("#auto_make").trigger("change");
    // });
    // $("#auto_make").change(function() {
        // var t = $("#auto_make").find(":selected").text();
        // var n = $("#auto_model");
        // n.empty();
        // $.each(e, function() {
            // if (this.make == t) {			
                // n.append($("<option />").val(this.make).text(this.model))
            // }
        // });
        // $("#auto_model").trigger("change");
    // })
	
	//set the MovingQuote href in nav menu
	if ( window.location.pathname == '/' ){
		// Index (home) page
		$('ul.menu li:last a').attr("href", "#form-area"); 
		$('ul.footer-menu li:last a').attr("href", "#form-area");
	} 
	
	$(".start_form").click(function() {
		$('html, body').animate({
			scrollTop: $("#multi-part-form").offset().top - 180
		}, 300);
		//if user has already started form - don't trigger the next button
		if(!form_values){
			$("#start_quote").trigger("click");
			
		}	
	});	
	
	$("#start_quote").click(function() {
		if(!form_values){
			//sleep(1000);
			//Command: toastr["success"]("Jennifer T. just found movers from TN to FL!");
		}			
	});	

});


function saveSteps(form_values,mpf_uid){
	var urlencoded = serializeUrl(form_values);
	console.log(ajaxurl+"action=save_mpf_steps&mpf_uid="+mpf_uid + "&" + urlencoded);
	$.ajax({
		url: ajaxurl,type:'GET',timeout:90000,
			dataType: 'html',
			data: "action=save_mpf_steps&mpf_uid="+mpf_uid + "&" + urlencoded,
			error: function(xml){
				//timeout, but not need to scare the user
				console.log(xml);
			},
			success: function(response){
				//hide any flag we have already shown
				console.log(response);
				try{
					save_response = $.parseJSON(response);						
				}
				catch (error) {
					save_response = false;
				}
				if(save_response){
					$.each(save_response, function() {
						console.log(save_response);
						if (this.flagged) {
							gtag('event', 'form_error', {'event_category': 'form_steps', 'event_label':'Error found in submitted form information: ' + this.html_id});		////analytics
							console.log('something flagged');
							$(this.html_id).addClass("flagged");
							if(this.html_id == '#email'){
								$("#mpf-response-9").html('Sorry, there was an error with that email address. Are you sure you typed it correctly?');
							} else if (this.html_id == '#phone_number'){
								$("#mpf-response-10").html('Sorry, there was an error with that phone. Are you sure you typed it correctly?');
							} else if (this.html_id == '#autocomplete'){
								$("#mpf-response-1").html('Sorry, there was an error with that address. Can you try again?');
							} else if (this.html_id == '#first_name'){
								$("#mpf-response-8").html('Sorry, there was an error with your name. Please confirm the information is correct?');
							} else if (this.html_id == '#move_date'){
								$("#mpf-response-5").html('Sorry, your move date must be sometime between tomorrow and 6 months away.');
							} else if (this.html_id == '#autocomplete2'){
								$("#mpf-response-2").html('Sorry, there was an error with that address. Can you try again?');
							} else if (this.html_id == '#auto_year'){
								$("#mpf-response-auto").html('Sorry, there was an error with that vehicle info. Can you try again?');
							} else if (this.html_id == '#first_name'){
								$("#mpf-response-8").html('Sorry, there was an error with the name provided. Can you try again?');
							} else if (this.html_id == '#consent_agree'){
								$("#consent-message").html(this.consent_message);
							}
						} else {
							$("#confirmed").addClass("allConfirmed");
							$("#confirmation_fieldset").removeClass('validation_waiting');
						}
					});	
					if(!form_values['auto_model'] && !all_done){
						//on final step - don't do auto next click
						//console.log('prgrm click next button now');
						$("#button_validate").trigger("click"); //when ready - auto proceed to next step	
						
					}
					if(all_done){
						//hide quote buttons
						ti_fa('form_submit_success'); //TruConversion success
						ga('send', {
						  hitType: 'event',
						  eventCategory: 'Form',
						  eventAction: 'success'
						});
						hideQuoteButtons();
					}
				}
			},
	});	
}

function getJson(e) {
    return JSON.parse($.ajax({
        type: "GET",
        url: e,
        dataType: "json",
        global: false,
        async: false,
        success: function(e) {
            return e
        }
    }).responseText)
}

function hideQuoteButtons(){
	$( ".start_form" ).remove();
	$('#menu-item-4131').remove();
	$('ul.footer-menu li:last a').remove();	
	$('#start_quote').remove();
	$("#mpf-response-0").html('We\'ve received your information and you should be hearing from a representative soon. Thanks!');
	
}

function checkZip(){
	$('#button2').addClass("disabled");  //temp disable next button
	var city = $('#locality2').val();
	var state = $('#administrative_area_level_12').val();
	
	if (!city)
		return;
	if  (!state)
		return;
	
	$.ajax({
		url: ajaxurl,type:'GET',timeout:5000,
			dataType: 'html',
			data: "action=check_zip&city="+city+"&state="+state,
			error: function(xml){
			},
			success: function(response){
				//hide any flag we have already shown
				if(response){
					$('#postal_code2').val(response);
					$('#button2').removeClass("disabled"); //temp disable next button			
				} else {
					//can't find zip
					$('fieldset:nth-child(3) .form-group').append('<label id="postal_code2-error" class="error" for="postal_code2" style="display: inline-block;">Sorry, we don\'t recognize that location. Can you try a zip code, or nearby city instead?</label>');
				}
			},
	});
}

function checkZipFrom(){
	$('#button1').addClass("disabled");  //temp disable next button
	var city = $('#locality').val();
	var state = $('#administrative_area_level_1').val();
	console.log('looking up for ' + city + ', ' + state);
	
	if (!city)
		return;
	if  (!state)
		return;
	
	$.ajax({
		url: ajaxurl,type:'GET',timeout:5000,
			dataType: 'html',
			data: "action=check_zip&city="+city+"&state="+state,
			error: function(xml){
			},
			success: function(response){
				//hide any flag we have already shown
				if(response){
					$('#postal_code').val(response);
					$('#button1').removeClass("disabled"); //temp disable next button			
				} else {
					//can't find zip
					$('fieldset:nth-child(2) .form-group').append('<label id="postal_code-error" class="error" for="postal_code" style="display: inline-block;">Please enter AND select a valid address (either zipcode or your full address) from the dropdown options.</label>');
				}
			},
	});
}

function recent_moves(){
	$.ajax({
		url: ajaxurl,type:'GET',timeout:5000,
			dataType: 'html',
			data: "action=recent_moves",
			error: function(xml){
					console.log(xml);
			},
			success: function(response){
				//hide any flag we have already shown
				if(response){
					recent_moves = JSON.parse(response);
				}
			},
	});
}

//used to serialize form object for ajax
function serializeUrl (obj) {
	// url ==> foo=2&foo=2,8&foo=2,10,3
	  var str = [];
	  for(var p in obj)
		if (obj.hasOwnProperty(p)) {
		  str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		}
	  return str.join("&");
}

////////////
//UID Sets//
function uniqueID(separator) {
    var delim = separator || "-";

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    var st = ((new Date).getTime().toString(16)).slice(0, 11) + (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1, 2);
    return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + st);
};


function setCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) 
{
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
// end UID Sets//



