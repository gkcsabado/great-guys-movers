
if($(window).width() < 576) {
	$("#collapse_services").removeClass("show");
}

$(window).resize(function() {
	if($(this).width() < 576) {
		$("#collapse_services").removeClass("show");
	} else {
		$("#collapse_services").addClass("show");
	}
});