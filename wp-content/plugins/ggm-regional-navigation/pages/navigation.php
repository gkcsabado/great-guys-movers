<?php



//preheader

function ggm_regional_navigation_preheader() {

	if  ( !is_admin() ) {

		global $post, $current_user;

		if ( !empty( $post->post_content ) && strpos ( $post->post_content, "[ggm_regional_navigation]" ) !== false) {

			

		}

	}

}

add_action( 'wp', 'ggm_regional_navigation_preheader', 1 );



//shortcode [ggm_regional_navigation]

function ggm_regional_navigation_shortcode($atts) {

	global $post;

	extract(shortcode_atts(array(

      // 'opening_headline' => 'Get Pricing Now',

      // 'opening_sub_headline' => '',

	  // 'opening_button_text' => 'Get Quote'

   ), $atts));	

   

   

	ob_start();

	

	//begin HTML for navigation below

	?>

<?php 

	$moving_guide_types = 'moving_guide_types';
	$regions = 'regions';

	$moving_guide_type = wp_get_object_terms( $post->ID,  $moving_guide_types );

	$nav_region = wp_get_object_terms( $post->ID,  $regions );

	//region has both region & state (midwest/illinois), need to get "illinois" for further filtering

	foreach($nav_region as $region_taxonomy){

		if($region_taxonomy->parent > 0){

			$state_slug = $region_taxonomy->slug;

			$state_name = $region_taxonomy->name;

		}

	}

	

	//don't know how this got here, was this just a sample code? commented out for now - 10/10/19

	//$loop = new WP_Query( array( 'post_type' => 'workshops', 'posts_per_page' => -1, 'category_name' => 'past-event' ) );



	$state_guide = array( 
		'post_type' 		=> 'moving_guides', 
		$regions 			=> $state_slug,
		$moving_guide_types => 'state'

	);

	$state_guide = new WP_Query( $state_guide );

	wp_reset_postdata();


	//for city guides, are we wanting to get {city} movers, or moving to {city} pages
	
	if($moving_guide_type[0]->slug  == 'city-movers'){
		$city_guide_type_lookup_slug = 'city-movers';
	} else {
		$city_guide_type_lookup_slug = 'city';
	}


	$city_guides = array( 
		'posts_per_page' 		=> 100,
		'post_type' 			=> 'moving_guides', 
		$regions 				=> $state_slug,
		'order' 				=> 'asc',
		'orderby' 				=> 'title',
		$moving_guide_types 	=> $city_guide_type_lookup_slug
	);

	$city_guides = new WP_Query( $city_guides );	

	wp_reset_postdata();


	$city_movers = array(
		'posts_per_page' 		=> 100,
		'post_type' 			=> 'moving_guides', 
		$regions 				=> $state_slug,
		'order' 				=> 'asc',
		'orderby' 				=> 'title',		
		$moving_guide_types 	=> 'city-movers'
	);

	$city_movers = new WP_Query( $city_movers );	

	wp_reset_postdata();
	

	if($moving_guide_type[0]->slug != "city-movers"){

		$from_to_guides = array( 
			'posts_per_page' 	=> 100,
			'post_type' 		=> 'moving_guides', 
			$regions 			=> $state_slug,
			$moving_guide_types => 'moving-from-to'
		);

		$from_to_guides = new WP_Query( $from_to_guides );	

		wp_reset_postdata();

	} else {

		//return empty so we don't show on city movers page

		$from_to_guides = array( 
			'post_type' 		=> 'moving_guides', 
			$regions 			=> 'lsdjfljf',
			$moving_guide_types => 'moving-from-to'
		);

		$from_to_guides = new WP_Query( $from_to_guides );	

		wp_reset_postdata();

	}	

	

	if($moving_guide_type[0]->slug != "city-movers"){
		$menu_headline = 'Related ' . $state_name . ' Moving Guides';
	} else {
		$menu_headline = 'Nearby Movers';
	}

	

	if ( 

		( $moving_guide_type[0]->slug != "city-movers" && ( $state_guide->have_posts() || $city_guides->have_posts() || $from_to_guides->have_posts() ) ) ||

		

		( $moving_guide_type[0]->slug == "city-movers" && $city_guides->have_posts()  ) 

		) : 

		?>


		<div id="rel_moving_guide_menu" class="services-menu">
			<div class="card">
				<div class="card-header" id="heading_services" data-toggle="collapse" data-target="#collapse_services" aria-expanded="true" aria-controls="collapse_services">
					<h4 class="mt-1 mb-1"><?php echo $menu_headline;?></h4>
				</div>

				<div id="collapse_services" class="collapse show" aria-labelledby="heading_services" data-parent="#rel_moving_guide_menu">
					<div class="card-body">
						<ul class="menu">
							<?php

							while ( $state_guide->have_posts() ) : $state_guide->the_post();
								
								$title = get_the_title();
								$state = str_replace(" Moving Guide", "", $title);
								$state_title = "Moving to " . $state;

								?>

								<li class="menu-item menu-item-type-post_type menu-item-object-page">
									<a href="<?= the_permalink(); ?>">
										<?php if(has_post_thumbnail()) { ?>
												<img class="img-services" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="" />
										<?php } else { ?>
												<img class="img-services" src="<?= plugins_url(); ?>/images/image-placeholder.png" alt="" />
										<?php } ?>
										<span class="text-services"><?= $state_title; ?></span>
									</a>
								</li>

								<?php

							endwhile;


							while ( $city_guides->have_posts() ) : $city_guides->the_post();

								$title = get_the_title();
								$city = str_replace(" Moving Guide", "", $title);

								if(strpos($city, ",")) {
									$arr = explode(",", $city);
									$city = $arr[0];
								}
								
								$city_title = "Moving to " . $city;

								?>

								

								<li class="menu-item menu-item-type-post_type menu-item-object-page">
									<a href="<?= the_permalink(); ?>">
										<?php if(has_post_thumbnail()) { ?>
												<img class="img-services" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="" />
										<?php } else { ?>
												<img class="img-services" src="<?= plugins_url(); ?>/images/image-placeholder.png" alt="" />
										<?php } ?>
										<span class="text-services"><?= $city_title; ?></span>
									</a>
								</li>

								<?php

							endwhile;

	
							while ( $from_to_guides->have_posts() ) : $from_to_guides->the_post();

								?>

								<li class="menu-item menu-item-type-post_type menu-item-object-page">
									<a href="<? the_permalink(); ?>">
										<?php if(has_post_thumbnail()) { ?>
												<img class="img-services" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="" />
										<?php } else { ?>
												<img class="img-services" src="<?= plugins_url(); ?>/images/image-placeholder.png" alt="" />
										<?php } ?>
										<span class="text-services"><?= the_title(); ?></span>
									</a>
								</li>

								<?php

							endwhile;

							?>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<?php

	endif;



	if ( $city_movers->have_posts() && $moving_guide_type[0]->slug != 'city-movers' ) : 

		?>

		<div id="" class="services-menu" style="margin-top:20px;">

			<h2><?php echo $state_name . ' Moving Companies' ;?></h2>

			<ul class="menu">

			<?php

			

			while ( $city_movers->have_posts() ) : $city_movers->the_post();

				$title = get_the_title();

				$city = str_replace(" Movers", "", $title);

				$city_title = $city . ' Movers';

				?>

				

				<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<? the_permalink(); ?>"><? the_title() ?></a></li>

				<?php

			endwhile;

			?>

			</ul>

		</div>

		<?php

	endif;	

	

?>

	

	

	<?php

	//Handle html from moving quote

	$temp_content = ob_get_contents();

	ob_end_clean();

	return $temp_content;

}

add_shortcode( 'ggm_regional_navigation', 'ggm_regional_navigation_shortcode' );





////////////////////////////////////

//////////FAQSCHEMA////////////////

//////DISABLED ON 11/15/19 //////

//// TO ENABLE AGAIN, SIMPLY UNCOMMENT THE ADD_ACTION() AT THE END OF THE BELOW CODE.

function add_moving_guide_schema(){

	global $post;

	$moving_guide_type = wp_get_object_terms( $post->ID,  'moving-guide-types' );

	

	if($moving_guide_type[0]->slug == "city"){ 

			

		?>

		

			<script>

			$(document).ready(function(){

				//get variables

				var faq1 = $('h1:eq(0)').text();

				faq1 = faq1 + "?";

				var answer1 = $('h1:eq(0)').next().html();

				var faq2 = $('.vc_col-md-8:eq(1) h2').text();

				var answer2 = $('.vc_col-md-8:eq(1) h2').nextAll();

				var answer2Html = "";

				for (var i = 0; i < answer2.length; i++) {

					answer2Html = answer2Html + $(answer2)[i].outerHTML;

				}

				var faq3 = $('.vc_col-md-8:eq(2) h2').text();

				var answer3 = $('.vc_col-md-8:eq(2) h2').nextAll();

				var answer3Html = "";

				for (var i = 0; i < answer3.length; i++) {

					answer3Html = answer3Html + $(answer3)[i].outerHTML;

				}

				var obj = {

				  "@context": "https://schema.org",

				  "@type": "FAQPage",

				  "mainEntity": [{

					"@type": "Question",

					"name": faq1,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer1

					}

				  }, {

					"@type": "Question",

					"name": faq2,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer2Html

					}

				  }, {

					"@type": "Question",

					"name": faq3,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer3Html

					}

				  }]

			  };

				

				var el = document.createElement('script');

				el.type = 'application/ld+json';

				el.text = JSON.stringify(obj);



				document.querySelector('head').appendChild(el);

			});

			</script>	

		

	<?php	

		} else if($moving_guide_type[0]->slug == "state" ) {

		

		?>

			<script>

			$(document).ready(function(){

				//get variables

				var faq1 = $('h1:eq(0)').text();

				faq1 = faq1 + "?";

				var answer1 = $('h1:eq(0)').next().html();

				var faq2 = $('.vc_col-md-8:eq(1) h2').text();

				var answer2 = $('.vc_col-md-8:eq(1) h2').nextAll();

				var answer2Html = "";

				for (var i = 0; i < answer2.length; i++) {

					answer2Html = answer2Html + $(answer2)[i].outerHTML;

				}

				var faq3 = $('.vc_col-md-8:eq(2) h2').text();

				var answer3 = $('.vc_col-md-8:eq(2) h2').nextAll();

				var answer3Html = "";

				for (var i = 0; i < answer3.length; i++) {

					answer3Html = answer3Html + $(answer3)[i].outerHTML;

				}

				var faq4 = $('.vc_col-md-8:eq(3) h2').text();

				var answer4 = $('.vc_col-md-8:eq(3) h2').nextAll();

				var answer4Html = "";

				for (var i = 0; i < answer4.length; i++) {

					answer4Html = answer4Html + $(answer4)[i].outerHTML;

				}

				var obj = {

				  "@context": "https://schema.org",

				  "@type": "FAQPage",

				  "mainEntity": [{

					"@type": "Question",

					"name": faq1,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer1

					}

				  }, {

					"@type": "Question",

					"name": faq2,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer2Html

					}

				  }, {

					"@type": "Question",

					"name": faq3,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer3Html

					}

				  }, {

					"@type": "Question",

					"name": faq4,

					"acceptedAnswer": {

					  "@type": "Answer",

					  "text": answer4Html

					}

				  }]

			  };

				

				var el = document.createElement('script');

				el.type = 'application/ld+json';

				el.text = JSON.stringify(obj);



				document.querySelector('head').appendChild(el);

			});

			</script>	

	<?php	

		}	

}	

//add_action('wp_footer', 'add_moving_guide_schema'); #DISABLING ON 11-15-19, issues losing traffic in serps (rand fishkin 0-click search presentation)



//////////FAQSCHEMA////////////////

///////////////////////////////////

