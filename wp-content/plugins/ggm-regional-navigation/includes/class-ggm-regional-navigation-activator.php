<?php
/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    GGM_Regional_Navigation
 * @subpackage GGM_Regional_Navigation/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    GGM_Regional_Navigation
 * @subpackage GGM_Regional_Navigation/includes
 * @author     GGM
 */
class GGM_Regional_Navigation_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}
}
