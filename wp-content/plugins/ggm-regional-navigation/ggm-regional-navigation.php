<?php
/**
 * The plugin bootstrap file.
 *
 * @package GGM_Regional_Navigation
 *
 * Plugin Name: GGM Regional Navigation
 * Plugin URI: #
 * Description: Navigation for inner region pages
 * Version: 1.0.0
 * Author: GGM
 * Author URI: #
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: ggm-regional-navigation
 */

/*
Copyright 2019 GGM (email : <email>)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// define
define( 'GGM_REGIONAL_NAVIGATION_DIRNAME', dirname( __FILE__ ));


/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ggm-regional-navigation-activator.php
 */
function activate_ggm_regional_navigation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ggm-regional-navigation-activator.php';
	GGM_Regional_Navigation_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ggm-regional-navigation-deactivator.php
 */
function deactivate_ggm_regional_navigation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ggm-regional-navigation-deactivator.php';
	GGM_Regional_Navigation_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ggm_regional_navigation' );
register_deactivation_hook( __FILE__, 'deactivate_ggm_regional_navigation' );


// shortcode for inserting moving quote

require_once GGM_REGIONAL_NAVIGATION_DIRNAME . "/pages/navigation.php"; 


function grn_load_styles() {
	if ( is_admin() ) {
		
	} else {
		wp_enqueue_style(
			'ggm-regional-navigation-plugin-style',
			plugins_url( 'css/style.css', __FILE__ ),
			array(),
			'screen'
		);		
	}	
}
add_action( 'init', 'grn_load_styles' );

function grn_load_scripts() {
	if ( is_admin() ) {	

	} else {
		wp_enqueue_script(
			'ggm-regional-navigation-plugin-custom',
			plugins_url( 'js/custom.js', __FILE__ ),
			array( 'jquery' ),
			null,
			true
		);			
	}
}
add_action( 'wp_enqueue_scripts', 'grn_load_scripts' );

function ggm_regional_nav_get_local_file_contents( $file_path ) {

    ob_start();

    include $file_path;

    $contents = ob_get_clean();

    return $contents;

}
