if(!getCookie('mpf_uid')){
	var mpf_uid = uniqueID();
	setCookie('mpf_uid',uniqueID(),7);
} else {
	var mpf_uid = getCookie('mpf_uid');
}

var zipcode_from = "";
var zipcode_to = "";
var datetimepicker = "";
var move_size = "";
var auto_year = "";
var auto_make = "";
var auto_model = "";
var auto_running = "";
var fname = "";
var lname = "";
var email = "";
var phone = "";

gtag('config', 'GA_TRACKING_ID', {
  'user_id': mpf_uid
});

gtag('config', 'AW-704839009');

$(".get-started-btn").attr({
	'data-toggle': 'modal',
	'data-target': '#multipart-form-modal',
	'data-initialstep': '#step_zipcode_from'
});

jQuery.fn.ForceNumericOnly =
function()
{
    return this.each(function()
    {
        $(this).keydown(function(e)
        {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, f5, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 116 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

$("#zipcode_from_main, #zipcode_from_sticky, #zipcode_from_cta").ForceNumericOnly();

$('#zipcode_from_main').keyup(function(){
	if($(this).val().length != 0){
		$('.form-error[data-field="zipcode_from_main"]').html('');
	} else {
		$('.form-error[data-field="zipcode_from_main"]').html($('#zipcode_from_main').data('errtext'));
	}
});

$(".btn-get-quote, .input-btn, .get-started-btn, .btn-zip-cta").click(function(e) {
	e.preventDefault();

	var currentstep = 1;
	var numberOfSteps = 0;

	if($(this).hasClass('btn-get-quote') || $(this).hasClass('get-started-btn') || $(this).hasClass('input-btn') || $(this).hasClass('btn-zip-cta')) {
		if($("#zipcode_from_main").val().length == 0 && $(this).hasClass('btn-get-quote')) {
			$('.form-error[data-field="zipcode_from_main"]').html($('#zipcode_from_main').data('errtext'));
		} else if(($("#zipcode_from_sticky").val().length == 0 && $(this).hasClass('input-btn')) || ($("#zipcode_from_cta").val().length == 0 && $(this).hasClass('btn-zip-cta'))) {
			// $('.form-error[data-field="zipcode_from_main"]').html($('#zipcode_from_main').data('errtext'));
		} else {

			$('body').append(`
			<div class="modal fade" id="multipart-form-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="multipart-form-modalLabel">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header flex-column">
							<!-- <h4 class="modal-title js-title-step"></h4>
							<p class="js-subtitle-step"></p> -->
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true"><i class="fa fa-close"></i></span>
							</button>
						</div>
						<div class="modal-body">
							<div class="qform-container">
								<div class="step-bar">
									<div class="step-progress"></div>
								</div>
								<form autocomplete="off" id="multipart-form">
									<div class="container">
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="1" id="step_zipcode_from">
											<div class="qform-header">
												<h4 class="qform-title">Let's get started.</h4>
												<p class="qform-subtitle mb-4">Where are you moving from?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<input type="tel" class="form-control" id="zipcode_from" name="zipcode_from" aria-describedby="zipcode_from_label" placeholder="Zip code" maxlength="5">
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="1">Back</button>
												<button type="button" class="btn btn-next" data-step="1">Next</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="2" id="step_zipcode_to">
											<div class="qform-header">
												<h4 class="qform-title">Great! We'd love to help you with your move.</h4>
												<p class="qform-subtitle mb-4">Where are you moving to?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<input type="tel" class="form-control" id="zipcode_to" name="zipcode_to" aria-describedby="zipcode_to_label" placeholder="Zip code" maxlength="5">
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="2">Back</button>
												<button type="button" class="btn btn-next" data-step="2">Next</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="3">
											<div class="qform-header">
												<h4 class="qform-title">Title 3</h4>
												<p class="qform-subtitle mb-4">What is the move date?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg datetimepicker-wrapper">
													<input type="text" id="datetimepicker" class="form-control" name="datetimepicker" aria-label="Large" aria-describedby="datetimepicker_label" placeholder="Move date" data-toggle="datetimepicker" data-target="#datetimepicker">
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="3">Back</button>
												<button type="button" class="btn btn-next" data-step="3">Check Availability</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="4">
											<div class="qform-header">
												<h4 class="qform-title">Good news! We can help you move with that date.</h4>
												<p class="qform-subtitle mb-4">What do you need to move?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<select name="move_size" id="move_size" class="form-control">
														<option value="" disabled>Choose a move size</option>
														<option value="1 Bedroom"> 1 Bedroom</option>
														<option value="2 Bedrooms" selected> 2 Bedrooms</option>
														<option value="3 Bedrooms"> 3 Bedrooms</option>
														<option value="4 Bedrooms"> 4 Bedrooms</option>
														<option value="5 Bedrooms"> 5 Bedrooms</option>
														<option value="Container"> Portable Storage Container</option>
														<option value="Piano"> Piano Only</option>
														<option value="Pool Table"> Pool Table Only</option>
														<option value="Appliances"> Appliances Only</option>
														<option value="Art"> Art or Antiques</option>
														<option value="Auto"> Auto Only</option>
													</select>
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="4">Back</button>
												<button type="button" class="btn btn-next" data-step="4">Next</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="5">
											<div class="qform-header">
												<h4 class="qform-title">Want to ship your car?</h4>
												<p class="qform-subtitle mb-4">Check affordable rates.</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<select name="auto_year" id="auto_year" class="form-control">
														<option value="" disabled selected>Choose a year</option>
													</select>
												</div>
												<div class="input-group input-group-lg">
													<input type="text" id="auto_make" class="form-control" name="auto_make" aria-label="Make" aria-describedby="auto_make_label" placeholder="Make (Example: Toyota)">
												</div>
												<div class="input-group input-group-lg">
													<input type="text" id="auto_model" class="form-control" name="auto_model" aria-label="Model (Ex. Camry)" aria-describedby="auto_model_label" placeholder="Model (Example: Camry)">
												</div>
												<div class="input-group auto-running-wrapper">
													<input type="checkbox" id="auto_running" class="mr-2" name="auto_running" checked>
													<label class="form-check-label" for="auto_running" style="line-height: 26px">
														Is vehicle running?
													</label>
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="5">Back</button>
												<button type="button" class="btn btn-next" data-step="5">Next</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="6">
											<div class="qform-header">
												<h4 class="qform-title">Title 6</h4>
												<p class="qform-subtitle mb-4">Do we have that right?</p>
											</div>
											<!-- <div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<input type="text" id="fname" class="form-control" name="fname" aria-label="First Name" aria-describedby="fname_label" placeholder="First name">
												</div>
												<div class="input-group input-group-lg">
													<input type="text" id="lname" class="form-control" name="lname" aria-label="Last Name" aria-describedby="lname_label" placeholder="Last name">
												</div>
											</div> -->
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light text-normal" data-step="6">NO! Not quite.</button>
												<button type="button" class="btn btn-next text-normal" data-step="6">YEP! That's right.</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="7" id="step_name">
											<div class="qform-header">
												<h4 class="qform-title" id="fname_title">Okay. Glad we got all the details right.</h4>
												<p class="qform-subtitle mb-4">What is your name?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<input type="text" id="fname" class="form-control" name="fname" aria-label="First Name" aria-describedby="fname_label" placeholder="First name">
												</div>
												<div class="input-group input-group-lg">
													<input type="text" id="lname" class="form-control" name="lname" aria-label="Last Name" aria-describedby="lname_label" placeholder="Last name">
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="7">Back</button>
												<button type="button" class="btn btn-next" data-step="7">Next</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="8" id="step_email">
											<div class="qform-header">
												<h4 class="qform-title" id="email_title">Title 8</h4>
												<p class="qform-subtitle mb-4">What is your preferred email?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<input type="email" id="email" name="email" class="form-control" aria-label="Email" aria-describedby="move_date_label" placeholder="Email address">
												</div>
											</div>
											<div class="qform-footer pt-4">
												<button type="button" class="btn btn-prev btn-light" data-step="8">Back</button>
												<button type="button" class="btn btn-next" data-step="8">Next</button>
											</div>
										</div>
										<div class="row pl-0 pr-0 pb-0 pl-sm-3 pr-sm-3 pb-sm-3" data-step="9" id="step_phone">
											<div class="qform-header">
												<h4 class="qform-title" id="phone_title">Great!</h4>
												<p class="qform-subtitle mb-4">What is the best number to reach you at?</p>
											</div>
											<div class="qform-content mb-3">
												<div class="input-group input-group-lg">
													<input type="tel" id="phone" name="phone" class="form-control" aria-label="Phone" aria-describedby="phone_label" placeholder="(555) 555-5555">
												</div>
											</div>
											<div class="qform-footer pt-4" >
												<button type="button" class="btn btn-prev btn-light" data-step="last">Back</button>
												<button type="button" class="btn btn-next" data-step="last">Complete</button>
												<div class="tos">
													<small id="phone-help" class="form-text text-muted">Please Note: Our moving specialist partners may contact you by text message, email, and/or phone (possibly with auto-dialing or recorded message) to get in touch with you to provide a moving quote. By continuing, you agree to be contacted about your move.</small>
												</div>
											</div>
										</div>
										<div class="row" data-step="10" id="step_loading">
											<div class="qform-header mb-4">
												<h4 class="qform-title">Please wait...</h4>
												<p class="qform-subtitle"></p>
											</div>
											<div class="qform-content mb-4">
												<div class="loader"></div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>`);

			$('#multipart-form-modal').modal();

			gtag('event', 'form_start', {'event_category': 'form_steps'}); // analytics

			$($(this).data('initialstep') + ' .btn-prev').addClass("btn-disabled").attr('disabled', 'disabled');
			$($(this).data('initialstep')).addClass('row-active');

			// subtract 1 because "auto" step not yet included
			numberOfSteps = $($(this).data('initialstep')).eq(0).nextAll(".row").length - 1;

			$($(this).data('initialstep')).parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });

			if(numberOfSteps == 7) {
				if($(this).hasClass("btn-get-quote")) {
					zipcode_from = $.trim($('#zipcode_from_main').val());
				} else {
					if($(this).hasClass("input-btn")) {
						zipcode_from = $.trim($('#zipcode_from_sticky').val());
					} else {
						zipcode_from = $.trim($('#zipcode_from_cta').val());
					}

					gtag('event', 'form_move_from', {'event_category': 'form_steps', 'event_label':zipcode_from}); // analytics
				}

				// custom conversational title
				$(".row[data-step=2] .qform-title").html("Great! We'd love to help you with your move from "+zipcode_from+".");
			}
		}
		

		$('#multipart-form-modal').on('shown.bs.modal', function(){
			// initAutocomplete();
			// $( "#from-autocomplete" ).focus(function() {
			// 	geolocate();
			// });

			// $.validate({
			// 	lang: 'en'
			// });

			$("#multipart-form").validate({
				rules: {
					zipcode_from: {
						required: true,
						zipcodeUS: true
					},
					zipcode_to: {
						required: true,
						zipcodeUS: true
					},
					datetimepicker: {
						required: true,
						dateITA: true
					},
					move_size: {
						required: true
					},
					auto_year: {
						required: true
					},
					auto_make: {
						required: true
					},
					auto_model: {
						required: true
					},
					fname: {
						required: true,
						minlength: 2
					},
					lname: {
						required: true,
						minlength: 2
					},
					email: {
						required: true,
						email: true
					},
					phone: {
						required: true,
						phoneUS: true
					}
				},
				messages: {
					zipcode_from: {
						required: "Zip code is required",
						zipcodeUS: "Please enter a valid zip code"
					},
					zipcode_to: {
						required: "Zip code is required",
						zipcodeUS: "Please enter a valid zip code"
					},
					datetimepicker: {
						required: "Please choose a move date",
						dateITA: "Please enter a date in the format mm/dd/yyyy"
					},
					move_size: {
						required: "Please select the approximate size that best represents your move"
					},
					auto_year: {
						required: "Please choose a year"
					},
					auto_make: {
						required: "Make is required"
					},
					auto_model: {
						required: "Model is required"
					},
					fname: {
						required: "Sorry, we need your full first name",
						minlength: "First name must be at least 2 characters"
					},
					lname: {
						required: "Sorry, we need your full last name",
						minlength: "Last name must be at least 2 characters"
					},
					email: {
						required: "Sorry, we need your email to send you confirmation",
						email: "Please enter a valid email address"
					},
					phone: {
						required: "We need your number to have a licensed moving rep call you back",
						phoneUS: "Please enter a valid phone number"
					}
				}
			});

			/*-------------- STEP 1 --------------*/

			$("#zipcode_from").ForceNumericOnly();
			$( "#zipcode_from" ).trigger('focus');


			$(".btn-next[data-step='1']").click(function() {
				var btnNxt = $(this);

				if($('#zipcode_from').valid()) {
					zipcode_from = $.trim($('#zipcode_from').val());

					gtag('event', 'form_move_from', {'event_category': 'form_steps', 'event_label':zipcode_from}); // analytics

					btnNxt.parent().parent().removeClass("row-active");
					btnNxt.parent().parent().next().addClass("row-active");

					currentstep++;
					$('#zipcode_from').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });

					// custom conversational title
					$(".row[data-step=2] .qform-title").html("Great! We'd love to help you with your move from "+zipcode_from+".");

				}
			});

			/*---------- END OF STEP 1 ----------*/

			/*-------------- STEP 2 --------------*/

			$("#zipcode_to").ForceNumericOnly();
			$( "#zipcode_to" ).trigger('focus');


			$(".btn-next[data-step='2']").click(function() {
				var btnNxt = $(this);

				if($('#zipcode_to').valid()) {
					zipcode_to = $.trim($('#zipcode_to').val());

					gtag('event', 'form_move_to', {'event_category': 'form_steps', 'event_label':zipcode_to}); // analytics

					btnNxt.parent().parent().removeClass("row-active");
					btnNxt.parent().parent().next().addClass("row-active");

					currentstep++;
					$('#datetimepicker').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });

					// custom conversational title
					$(".row[data-step=3] .qform-title").html("Moving from "+zipcode_from+" to "+zipcode_to+"... No problem!");

				}
			});

			$(".btn-prev[data-step='2']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().addClass("row-active");

				currentstep--;
				$('#zipcode_from').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			/*---------- END OF STEP 2 ----------*/

			/*-------------- STEP 3 --------------*/

			$('#datetimepicker').datetimepicker({
				date: moment().add(14,'days').format("MM/DD/YYYY"),
				minDate: moment().format("MM/DD/YYYY"),
				format: 'MM/DD/YYYY',
			});

			$(".btn-next[data-step='3']").click(function() {
				var btnNxt = $(this);

				if($('#datetimepicker').valid()) {
					datetimepicker = $.trim($('#datetimepicker').val());

					gtag('event', 'form_move_date', {'event_category': 'form_steps', 'event_label':datetimepicker}); // analytics

					btnNxt.parent().parent().removeClass("row-active");
					btnNxt.parent().parent().next().addClass("row-active");

					currentstep++;
					$('#move_size').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });

				}
			});

			$(".btn-prev[data-step='3']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().addClass("row-active");

				currentstep--;
				$('#zipcode_to').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			/*---------- END OF STEP 3 ----------*/

			/*-------------- STEP 4 --------------*/

			// $('#move_size').select2();

			$(".btn-next[data-step='4']").click(function() {
				var btnNxt = $(this);

				if($('#move_size').valid()) {
					move_size = $('#move_size').val();

					gtag('event', 'form_move_size', {'event_category': 'form_steps', 'event_label':move_size}); // analytics

					btnNxt.parent().parent().removeClass("row-active");

					if(move_size === "Auto") {
						btnNxt.parent().parent().next().addClass("row-active");
						numberOfSteps++;

						gtag('event', 'form_auto_shown', {'event_category': 'form_steps'}); // analytics

						// custom conversational title
						$(".row[data-step=6] .qform-title").html("So, you're moving a car from "+zipcode_from+" to "+zipcode_to+" on "+datetimepicker+".");
					} else {
						btnNxt.parent().parent().next().next().addClass("row-active");

						// custom conversational title
						$(".row[data-step=6] .qform-title").html("So, you're moving "+move_size+" from "+zipcode_from+" to "+zipcode_to+" on "+datetimepicker+".");
					}

					currentstep++;
					$('#fname').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
					
				}
			});

			$(".btn-prev[data-step='4']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().addClass("row-active");

				currentstep--;
				$('#datetimepicker').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			/*---------- END OF STEP 4 ----------*/

			/*-------------- STEP 5 --------------*/

			for (i = new Date().getFullYear(); i > 1900; i--) {
				$('#auto_year').append($('<option />').val(i).html(i));
			}

			// $('#auto_year').select2();

			$(".btn-next[data-step='5']").click(function() {
				var btnNxt = $(this);

				if($('#auto_year, #auto_make, #auto_model').valid()) {
					auto_year = $('#auto_year').val();
					auto_make = $.trim($('#auto_make').val());
					auto_model = $.trim($('#auto_model').val());
					auto_running = $('#auto_running').val();

					gtag('event', 'form_submit_auto', {'event_category': 'form_steps'}); // analytics

					btnNxt.parent().parent().removeClass("row-active");
					btnNxt.parent().parent().next().addClass("row-active");

					currentstep++;
					$('#fname').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
				}
			});

			$(".btn-prev[data-step='5']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().addClass("row-active");

				if(move_size === "Auto") {
					numberOfSteps--;
				}

				currentstep--;
				$('#move_size').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			/*---------- END OF STEP 5 ----------*/

			/*-------------- STEP 6 --------------*/

			$(".btn-next[data-step='6']").click(function() {
				var btnNxt = $(this);

				btnNxt.parent().parent().removeClass("row-active");
				btnNxt.parent().parent().next().addClass("row-active");

				currentstep++;
				$('#fname').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			$(".btn-prev[data-step='6']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().prev().addClass("row-active");

				if(move_size === "Auto") {
					numberOfSteps--;
					currentstep--;
				}

				currentstep--;

				$('#move_size').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
				
			});

			/*---------- END OF STEP 6 ----------*/

			/*-------------- STEP 7 --------------*/

			$(".btn-next[data-step='7']").click(function() {
				var btnNxt = $(this);

				if($('#fname, #lname').valid()) {
					fname = $.trim($('#fname').val());
					lname = $.trim($('#lname').val());

					gtag('event', 'form_name', {'event_category': 'form_steps'}); // analytics

					btnNxt.parent().parent().removeClass("row-active");
					btnNxt.parent().parent().next().addClass("row-active");

					currentstep++;
					$('#email').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });

					// custom conversational title
					$(".row[data-step=8] .qform-title").html("Thanks <span class='text-capitalize'>"+fname+"</span>! We're almost finished.");
				} else {
					gtag('event', 'form_name_error', {'event_category': 'form_steps'}); // analytics
				}
			});

			$(".btn-prev[data-step='7']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().prev().prev().addClass("row-active");
				currentstep = currentstep - 2;

				if(move_size === "Auto") {
					numberOfSteps--;
					currentstep--;
				}

				$('#move_size').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
				
			});

			/*---------- END OF STEP 7 ----------*/

			/*-------------- STEP 8 --------------*/

			$(".btn-next[data-step='8']").click(function() {
				var btnNxt = $(this);

				if($('#email').valid()) {
					email = $.trim($('#email').val());

					gtag('event', 'form_email', {'event_category': 'form_steps'}); // analytics

					btnNxt.parent().parent().removeClass("row-active");
					btnNxt.parent().parent().next().addClass("row-active");

					currentstep++;
					$('#phone').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
				} else {
					gtag('event', 'form_email_error', {'event_category': 'form_steps'}); // analytics
				}
			});

			$(".btn-prev[data-step='8']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().addClass("row-active");

				currentstep--;
				$('#fname').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			/*---------- END OF STEP 8 ----------*/

			/*-------------- STEP 9 --------------*/

			$('#phone').mask('(000) 000-0000');

			$(".btn-next[data-step='last']").click(function() {
				var btnNxt = $(this);

				if($('#phone').valid()) {
					phone = $.trim($('#phone').val());
					phone = phone.replace(/\D/g,'');

					gtag('event', 'form_phone', {'event_category': 'form_steps'}); // analytics
					gtag('event', 'form_submit', {'event_category': 'form_steps'}); // analytics

					btnNxt.parent().parent().removeClass("row-active");

					$(".step-bar").hide();
					$(".modal-header").hide();

					submitForm();
				} else {
					gtag('event', 'form_phone_error', {'event_category': 'form_steps'}); // analytics
				}
			});

			$(".btn-prev[data-step='last']").click(function() {
				var btnPrev = $(this);
				btnPrev.parent().parent().removeClass("row-active");
				btnPrev.parent().parent().prev().addClass("row-active");

				currentstep--;
				$('#email').parent().parent().parent().parent().parent().prev().children().animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
			});

			/*---------- END OF STEP 9 ----------*/
	    });

		$('#multipart-form-modal').on('hide.bs.modal', function(){
	    	$("#zipcode_from_main, #zipcode_from_sticky, #zipcode_from_cta").val('');
	    });

	    $('#multipart-form-modal').on('hidden.bs.modal', function(){
	    	$(this).remove();
	    });
	}
});

function submitForm() {
	$('#phone').unmask();
	var form = $("#multipart-form").serialize();

	if($.trim($('#zipcode_from').val()).length > 0)
		form += '&zipcode_from_main=';
	else
		form += '&zipcode_from_main=' + zipcode_from;

	$.ajax({
		url: ajaxurl,
		type: 'POST',
		timeout: 50000,
		data: "action=save_mpf_steps&mpf_uid="+mpf_uid + "&" + form,
		error: function(err){
			console.log(err);
		},
		beforeSend: function() {
			$(".btn-next[data-step='last']").parent().parent().next().addClass("row-active");
        },
        complete: function(){
			
        },
		success: function(data){
			// console.log(data);
			var save_response = '';
			try{
				save_response = $.parseJSON(data);						
			}
			catch (error) {
				save_response = false;
			}
			
			if(save_response){
				// console.log(save_response);
				if(save_response[0].flagged) {
					var rowID = ''
					var priority = 0;

					$(".step-bar").show();
					$(".modal-header").show();

					$.each(save_response, function() {
						gtag('event', 'form_error', {'event_category': 'form_steps', 'event_label':'Error found in submitted form information: ' + this.html_id}); // analytics

						gtag('event', 'form_error_response_time', {'event_category': 'form_steps', 'event_label':this.response_time}); // analytics

						if(this.html_id == '#email'){
							$("#email_title").html('Sorry, there was an error with that email address. Are you sure you typed it correctly?');
							if(priority < 2)
								rowID = "#step_email";
						} else if (this.html_id == '#phone_number'){
							$("#phone_title").html('Sorry, there was an error with that phone. Are you sure you typed it correctly?');
							if(priority < 1)
								rowID = "#step_phone";
						} else if (this.html_id == '#first_name'){
							$("#fname_title").html('Sorry, there was an error with the name provided. Can you try again?');
							if(priority < 3)
								rowID = "#step_name";
						} else {

						}

						if(this.flagged) {

						} else {
							gtag('event', 'form_success_response_time', {'event_category': 'form_steps', 'event_label':this.response_time}); // analytics

							gtag('event', 'conversion', {'send_to': 'AW-123456789/AbC-D_efG-h12_34-567','value': this.response_time,'currency': 'USD'}); // analytics
						}
					});

					$("#step_loading").removeClass('row-active');
					$(rowID + ' .btn-prev').addClass("btn-disabled").attr('disabled', 'disabled');
					$(rowID + ' .btn-next').addClass("btn-flagged");
					$(rowID).addClass('row-active');
					$(".step-progress").css('width', '0%');

					currentstep = 1;
					numberOfSteps = $(rowID).eq(0).nextAll(".row").length;

					$(".step-progress").animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });

					$('.btn-next, .btn-prev').addClass("btn-flagged");

					/*-------------- NEW STEP 7 --------------*/

					$(".btn-next.btn-flagged[data-step='7']").unbind("click").bind("click", function(){
						var btnNxt = $(this);

						if($('#fname, #lname').valid()) {
							fname = $.trim($('#fname').val());
							lname = $.trim($('#lname').val());

							btnNxt.parent().parent().removeClass("row-active");
							btnNxt.parent().parent().next().addClass("row-active");

							currentstep++;
							$(".step-progress").animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
						}
					});

					/*---------- END OF NEW STEP 7 ----------*/

					/*-------------- NEW STEP 8 --------------*/

					$(".btn-next.btn-flagged[data-step='8']").unbind("click").bind("click", function(){
						var btnNxt = $(this);

						if($('#email').valid()) {
							email = $.trim($('#email').val());

							btnNxt.parent().parent().removeClass("row-active");
							btnNxt.parent().parent().next().addClass("row-active");

							currentstep++;
							$(".step-progress").animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
						}
					});

					$(".btn-prev.btn-flagged[data-step='8']").unbind("click").bind("click", function(){
						var btnPrev = $(this);
						btnPrev.parent().parent().removeClass("row-active");
						btnPrev.parent().parent().prev().addClass("row-active");

						currentstep--;
						$(".step-progress").animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
					});

					/*---------- END OF NEW STEP 8 ----------*/

					/*-------------- NEW STEP 9 --------------*/

					$('#phone').mask('(000) 000-0000');

					$(".btn-next.btn-flagged[data-step='last']").unbind("click").bind("click", function(){
						var btnNxt = $(this);

						if($('#phone').valid()) {
							phone = $.trim($('#phone').val());
							phone = phone.replace(/\D/g,'');

							btnNxt.parent().parent().removeClass("row-active");

							$(".step-bar").hide();
							$(".modal-header").hide();

							submitForm();
						}
					});

					$(".btn-prev.btn-flagged[data-step='last']").unbind("click").bind("click", function(){
						var btnPrev = $(this);
						btnPrev.parent().parent().removeClass("row-active");
						btnPrev.parent().parent().prev().addClass("row-active");

						currentstep--;
						$(".step-progress").animate({ 'width' : (currentstep / numberOfSteps) * 100 + '%' });
					});

					/*---------- END OF NEW STEP 9 ----------*/


				} else {
					$('#multipart-form-modal').modal('hide');

					Swal.fire(
						'Thanks! Everything looks good.',
						'You will be hearing from a licensed moving rep soon.',
						'success'
					);
				}
			} else {
				$('#multipart-form-modal').modal('hide');

				Swal.fire(
					'Oops...',
					'Something went wrong!',
					'error'
				);
			}
			
		},

	});	
}


//--------------- Unique ID -----------------//

function uniqueID(separator) {
    var delim = separator || "-";

    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }

    var st = ((new Date).getTime().toString(16)).slice(0, 11) + (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1, 2);
    
    return (S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + st);
};

function setCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";

    document.cookie = name+"="+value+expires+"; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }

    return null;
}