
var placeSearch, autocomplete, autocomplete2;

var componentForm = {

	street_number: 'short_name',

	route: 'long_name',

	locality: 'long_name',

	administrative_area_level_1: 'short_name',

	country: 'long_name',

	postal_code: 'short_name'

};

function initAutocomplete() {



	

	// Create the autocomplete object, restricting the search to geographical

	// location types.

	autocomplete = new google.maps.places.Autocomplete(

		/** @type {!HTMLInputElement} */

		(document.getElementById('from-autocomplete')), {

			types: ['address'],
			componentRestrictions: { country: ["us","ca","mx"] },
			language: ['en']

		}

	);



	// When the user selects an address from the dropdown, populate the address

	// fields in the form.

	autocomplete.addListener('place_changed', function() {

		fillInAddress(autocomplete, "", 'from');

	});



	autocomplete2 = new google.maps.places.Autocomplete(

		/** @type {!HTMLInputElement} */

		(document.getElementById('autocomplete2')), {

			types: ['(regions)'],

			language: ['en']

		}

	);



	autocomplete2.addListener('place_changed', function() {

		fillInAddress(autocomplete2, "2", 'to');

	});		

}



function fillInAddress(autocomplete, unique, location_for) {

	

	console.log('looking up address for: ' + location_for);

	jQuery('#button2').removeClass("disabled"); //temp disable next button (if user selects location not found in zip lookup)

	jQuery('fieldset:nth-child(3) .form-group label').remove();		//also remove error message (wait til new suggestions arrive)	

	

	// Get the place details from the autocomplete object.

	var place = autocomplete.getPlace();

	var zip2set = false;



	for (var component in componentForm) {

		if (!!document.getElementById(component + unique)) {

			document.getElementById(component + unique).value = '';

			document.getElementById(component + unique).disabled = false;

		}

	}



	// Get each component of the address from the place details

	// and fill the corresponding field on the form.

	for (var i = 0; i < place.address_components.length; i++) {

		var addressType = place.address_components[i].types[0];

		if (componentForm[addressType] && document.getElementById(addressType + unique)) {

			var val = place.address_components[i][componentForm[addressType]];

			document.getElementById(addressType + unique).value = val;

		}

		if (place.address_components[i].types[0] == 'locality') {

			//need to trigger change since jquery won't register programattic .change()

			var city = val

			jQuery('#locality').trigger('change'); 

		}

		if (place.address_components[i].types[0] == 'locality2') {

			//need to trigger change since jquery won't register programattic .change()

			var city = val;

			jQuery('fieldset:nth-child(3) .form-group label').remove(); //city found - remove any existing error messages

			jQuery('#locality2').trigger('change'); 

		}

		if (place.address_components[i].types[0] == 'postal_code' || place.address_components[i].types[0] == 'postal_code2') {

				var zip2set = true;

		}

	}

	// if Geocode returns valid info, but no zip-code - trigger function to do ajax call for zip lookup

	if (!(zip2set) && city){

		if(location_for == "from"){

			gtag('event', 'form_backend_process', {'event_category': 'form_steps', 'event_label':'geo_checking our database for move from: ' + city}); ////analytics

			checkZipFrom();

		} else if(location_for == "to"){

			gtag('event', 'form_backend_process', {'event_category': 'form_steps', 'event_label':'geo_checking our database for move to: ' + city}); ////analytics

			checkZip();

		}

	} else if(!(zip2set) && !city){

		jQuery('fieldset:nth-child(3) .form-group').append('<label id="postal_code2-error" class="error" for="postal_code2" style="display: inline-block;">Sorry, there was an error with that location. Can you try using a zip code, or nearby city instead?</label>');

	}

	

	

}





// Bias the autocomplete object to the user's geographical location,

// as supplied by the browser's 'navigator.geolocation' object.

function geolocate() {

	

	if (navigator.geolocation) {

	  navigator.geolocation.getCurrentPosition(function(position) {

		var geolocation = {

		  lat: position.coords.latitude,

		  lng: position.coords.longitude

		};

		var circle = new google.maps.Circle({

		  center: geolocation,

		  radius: position.coords.accuracy

		});

		autocomplete.setBounds(circle.getBounds());

	  });

	}

}
