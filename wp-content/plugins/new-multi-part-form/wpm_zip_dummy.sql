INSERT INTO `wp_zipcodes` (`zip`, `city`, `state`, `latitude`, `longitude`, `timezone`, `dst`) VALUES
('00210', 'Portsmouth', 'NH', 43.0059, -71.0132, -5, 1),
('00211', 'Portsmouth', 'NH', 43.0059, -71.0132, -5, 1),
('00212', 'Portsmouth', 'NH', 43.0059, -71.0132, -5, 1),
('00213', 'Portsmouth', 'NH', 43.0059, -71.0132, -5, 1),
('00214', 'Portsmouth', 'NH', 43.0059, -71.0132, -5, 1),
('00215', 'Portsmouth', 'NH', 43.0059, -71.0132, -5, 1),
('00501', 'Holtsville', 'NY', 40.9223, -72.6371, -5, 1),
('00544', 'Holtsville', 'NY', 40.9223, -72.6371, -5, 1);