<?php
/**
 * The plugin bootstrap file.
 *
 * @package Multi_Part_Form
 *
 * Plugin Name: Multi-Part Form
 * Plugin URI: #
 * Description: A plugin that  has a multi-part moving form to submit to API.
 * Version: 1.0.0
 * Author: Moonraker
 * Author URI: #
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: multi-part-form
 */

/*
Copyright 2019 Moonraker (email : <email>)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Define.
define( 'MULTI_PART_FORM_NAME', 'Multi-Part Form' );
define( 'MULTI_PART_FORM_VERSION', '1.0.0' );
define( 'MULTI_PART_FORM_DIRNAME', dirname( __FILE__ ));
define( 'MULTI_PART_FORM_BASENAME', basename( dirname( __FILE__ ) ) );
define( 'MULTI_PART_FORM_DIR', rtrim( plugin_dir_path( __FILE__ ), '/' ) );
define( 'MULTI_PART_FORM_URL', rtrim( plugin_dir_url( __FILE__ ), '/' ) );


// ------------------------- START --------------------------- //

// Global Settings
global $mpf_settings;

$post;

$mpf_settings = array(
	'brain_api_username' => 'greatguysmoving',
	'brain_api_password' => '7ntfUMtdQH56CNdS96nV6P',
	'api_token_url' => 'http://159.203.75.204/twilio/app/users/gettoken.json',
	'api_submit_quote_url' => 'http://159.203.75.204/twilio/app/moving-leads/addapiconsent.json'
	//'api_submit_quote_url' => 'http://159.203.75.204/twilio/app/form-leads/addapi.json'
);


// Shortcode for inserting moving quote form
require_once MULTI_PART_FORM_DIRNAME . "/pages/quote.php";
require_once MULTI_PART_FORM_DIRNAME . "/services/mpf_ajax.php";
// require_once MULTI_PART_FORM_DIRNAME . "/pages/admin-settings.php";

function mq_load_styles() {
	if ( is_admin() ) {
		
	} else {
		wp_enqueue_style(
			'moving-quotes-plugin-select2',
			plugins_url( 'css/select2.min.css', __FILE__ ),
			array(),
			'screen'
		);		
		wp_enqueue_style(
			'moving-quotes-plugin-td-datetimepicker',
			plugins_url( 'css/td-datetimepicker.min.css', __FILE__ ),
			array(),
			'screen'
		);	
		wp_enqueue_style(
			'moving-quotes-plugin-frontend',
			plugins_url( 'css/frontend.css', __FILE__ ),
			array(),
			'screen'
		);		
	}	
}
add_action( 'init', 'mq_load_styles' );

function mq_load_scripts() {
	if ( is_admin() ) {	

	} else {
		wp_enqueue_script(
			'moving-quotes-plugin-google-places-api',
			'https://maps.googleapis.com/maps/api/js?key=AIzaSyAk4x_4j4RF9wsNzg0T4alrWTU3_N6Fjgg&libraries=places',
			// 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA2_g9Kr-vTfp5Cfo4_hbM9lUIT4kGQvQ8&libraries=places',
			array(),
			null,
			true
		);	
		wp_enqueue_script(
			//script for gmaps api
			'moving-quotes-plugin-auto-complete',
			plugins_url( 'js/google_maps_api_address.js', __FILE__ ),
			array( 'jquery', 'moving-quotes-plugin-google-places-api' ),
			null,
			true
		);	
		wp_enqueue_script(
			// moment
			'moving-quotes-plugin-moment',
			plugins_url( 'js/moment.min.js', __FILE__ ),
			array( 'child-jquery' ),
			null,
			false
		);
		wp_enqueue_script(
			// jquery.validate
			'moving-quotes-plugin-jquery-validator',
			plugins_url( 'js/jquery.validate.min.js', __FILE__ ),
			array( 'child-jquery' ),
			null,
			true
		);
		wp_enqueue_script(
			// jquery.validate
			'moving-quotes-plugin-jquery-validator-additional',
			plugins_url( 'js/additional-methods.min.js', __FILE__ ),
			array( 'moving-quotes-plugin-jquery-validator' ),
			null,
			true
		);
		wp_enqueue_script(
			// jquery mask
			'moving-quotes-plugin-jquery-mask',
			plugins_url( 'js/jquery.mask.min.js', __FILE__ ),
			array( 'child-understrap-scripts' ),
			null,
			true
		);
		wp_enqueue_script(
			// select2
			'moving-quotes-plugin-select2',
			plugins_url( 'js/select2.min.js', __FILE__ ),
			array( 'child-understrap-scripts' ),
			null,
			true
		);
		wp_enqueue_script(
			// datetimepicker
			'moving-quotes-plugin-td-datetimepicker',
			plugins_url( 'js/td-datetimepicker.min.js', __FILE__ ),
			array( 'child-understrap-scripts' ),
			null,
			true
		);
		wp_enqueue_script(
			// modal steps
			'moving-quotes-plugin-modal-steps',
			plugins_url( 'js/modal-steps.min.js', __FILE__ ),
			array( 'child-understrap-scripts' ),
			null,
			true
		);
		wp_enqueue_script(
			// swal
			'moving-quotes-plugin-swal',
			plugins_url( 'js/sweetalert.min.js', __FILE__ ),
			array( 'child-understrap-scripts' ),
			null,
			true
		);
		if( is_page( 8400 ) ){
			wp_enqueue_script(
				'moving-quotes-plugin-frontend-updated',
				plugins_url( 'js/frontend_updated.js', __FILE__ ),
				array( 'jquery' ),
				null,
				true
			);
		} else {
			wp_enqueue_script(
				'moving-quotes-plugin-frontend',
				plugins_url( 'js/frontend.js', __FILE__ ),
				array( 'jquery' ),
				null,
				true
			);		
		}	
	}
}
add_action( 'wp_enqueue_scripts', 'mq_load_scripts' );

/**
 * Filters just the chat-script
 */
add_filter( 'script_loader_tag', 'asyncdefer_tag', 10, 3 );
function asyncdefer_tag( $tag, $handle, $src ) {
	if ( $handle !== 'moving-quotes-plugin-google-places-api' ) {
		return $tag;
	}

	return "<script type='text/javascript' src='$src' async defer></script>\n";
}


// Add AJAX url as js var to header

function my_wp_head_ajax_url() {  ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url("admin-ajax.php");?>';
	</script>
<?php }

add_action('wp_head', 'my_wp_head_ajax_url');


/* Create table wp_zipcodes in db */
function zip_install () {

    global $wpdb;
    $table_name = $wpdb->prefix . "zipcodes";

    if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

	    $sql = "CREATE TABLE " . $table_name . " (

	        `zip` char(5) NOT NULL,

	        `city` varchar(64) DEFAULT NULL,

	        `state` char(2) DEFAULT NULL,

	        `latitude` float DEFAULT NULL,

	        `longitude` float NOT NULL,

	        `timezone` int(11) DEFAULT NULL,

	        `dst` int(20) NOT NULL,

	        PRIMARY KEY (`zip`)

	    );";

	    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	    dbDelta($sql);
    }

	//Do we need to import zipcodes?
	$count_query = "select count(*) from $table_name";
    $num = $wpdb->get_var($count_query);

	if($num < 40000){
		//try the sql way first, if it fails, go the php route
		$file_db = plugins_url('new-multi-part-form/wpm_zip_dummy.sql');
		$input_data_to_table = file_get_contents($file_db);

		if($input_data_to_table){
			$rows_affected = $wpdb->query( $input_data_to_table );
		} else {
			//force the php way in case there is a max size on MYSQL (common for CWP installs)
			require_once dirname( __FILE__ ) . "/wpm_zip_dummy.php";

			foreach ($zipcodes as $zipcode){
				$wpdb->insert(
					$table_name,
					array(
						'zip' => $zipcode[0],
						'city' => $zipcode[1],
						'state' => $zipcode[2],
						'latitude' => $zipcode[3],
						'longitude' => $zipcode[4],
						'timezone' => $zipcode[5],
						'dst' => $zipcode[6]		
					),
					array(
						'%s', 
						'%s', 
						'%s', 
						'%f',
						'%f',
						'%d',
						'%d'
					)
				);
			}	
		}		
	}
}

register_activation_hook( __FILE__, 'zip_install' );


/* Create table wp_car_models in db */

function cars_install () {
    global $wpdb;

    $table_name = $wpdb->prefix . "car_models";

    if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

	    $sql = "CREATE TABLE " . $table_name . " (

		  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,

		  `year` int(4) unsigned NOT NULL,

		  `make` varchar(50) DEFAULT NULL,

		  `model` varchar(50) NOT NULL,

		  PRIMARY KEY (`id`),

		  UNIQUE KEY `U_VehicleModelYear_year_make_model` (`year`,`make`,`model`),

		  KEY `I_VehicleModelYear_year` (`year`),

		  KEY `I_VehicleModelYear_make` (`make`),

		  KEY `I_VehicleModelYear_model` (`model`)

	    );";

	    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	    dbDelta($sql);
    }

	//Do we need to import zipcodes?

	$count_query = "select count(*) from $table_name";

    $num = $wpdb->get_var($count_query);

	if($num < 7000){
		//try the sql way first, if it fails, go the php route
		$file_db = plugins_url('new-multi-part-form/mpf_car_models_dummy.sql');
		$input_data_to_table = file_get_contents($file_db);

		if($input_data_to_table){
			$rows_affected = $wpdb->query( $input_data_to_table );
		}
	}
}

register_activation_hook( __FILE__, 'cars_install' );

/* Create table wp_mpf_leads in db */

function mpf_leads_db () {

    global $wpdb;

    $table_name = $wpdb->prefix . "mpf_leads";

    if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

		$sql = "CREATE TABLE " . $table_name . " (

			`id` int(20) NOT NULL AUTO_INCREMENT,

			`mpf_uid` varchar(86) NOT NULL UNIQUE,

			`date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,

			`city` varchar(64) DEFAULT NULL,

			`city2` varchar(64) DEFAULT NULL,

			`country` varchar(64) DEFAULT NULL,

			`country2` varchar(64) DEFAULT NULL,

			`email` varchar(85) DEFAULT NULL,

			`first_name` varchar(25) DEFAULT NULL,

			`last_name` varchar(25) DEFAULT NULL,

			`auto_make` varchar(50) DEFAULT NULL,

			`auto_model` varchar(50) DEFAULT NULL,

			`auto_year` varchar(4) DEFAULT NULL,

			`move_date` DATETIME DEFAULT NULL,

			`move_from_type` varchar(25) DEFAULT NULL,

			`move_to_type` varchar(25) DEFAULT NULL,

			`move_size` varchar(25) DEFAULT NULL,

			`international` tinyint(1) DEFAULT NULL,

			`interstate` tinyint(1) DEFAULT NULL,

			`phone_number` varchar(10) DEFAULT NULL,

			`postal_code` varchar(10) DEFAULT NULL,

			`postal_code2` varchar(10) DEFAULT NULL,

			`region` varchar(64) DEFAULT NULL,

			`region2` varchar(64) DEFAULT NULL,

			`route` varchar(85) DEFAULT NULL,

			`route2` varchar(85) DEFAULT NULL,

			`auto_running` tinyint(1) DEFAULT NULL,

			`street_number` char(25) DEFAULT NULL,

			`street_number2` char(25) DEFAULT NULL,

			`valid` tinyint(1) DEFAULT NULL,

			`sold` tinyint(1) DEFAULT NULL,

			`consent_agree` tinyint(1) DEFAULT 0,

			`page_url` varchar(99) DEFAULT NULL,

			PRIMARY KEY (`id`)

		);";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
    }
}
register_activation_hook( __FILE__, 'mpf_leads_db' );

/* Cleaning db after delete plugin */

function clean_after_deactivate( ) {
    global $wpdb;
    $wpdb->query('DROP TABLE IF EXISTS wp_zipcodes');
}



function get_local_file_contents( $file_path ) {
    ob_start();

    include $file_path;

    $contents = ob_get_clean();

    return $contents;
}