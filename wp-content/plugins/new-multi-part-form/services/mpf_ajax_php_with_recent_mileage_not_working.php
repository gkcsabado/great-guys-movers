<?php

function check_zip_callback() {
	global $wpdb;
	$city = $_REQUEST['city'];
	$state = $_REQUEST['state'];
	$zips = $wpdb->get_results( 'SELECT DISTINCT zip FROM wp_zipcodes WHERE city = "' . $city . '" AND state = "' . $state . '" Limit 1' );
	foreach($zips as $zip) echo $zip->zip;	
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_check_zip', 'check_zip_callback' );
add_action( 'wp_ajax_nopriv_check_zip', 'check_zip_callback' );

function car_models() {
	global $wpdb;
	$year = (ISSET($_REQUEST['year']) && $_REQUEST['year']!=='') ? $_REQUEST['year'] : "2013";
	
	$results = $wpdb->get_results( "SELECT DISTINCT id, make, model FROM wp_car_models WHERE wp_car_models.year='$year' ORDER BY make asc" );
	$vehicle_models = array();	
	foreach($results as $row){
		$vehicle_models[] = $row;
	} 		
	echo json_encode($vehicle_models);
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_car_models', 'car_models' );
add_action( 'wp_ajax_nopriv_car_models', 'car_models' );

function save_mpf_steps() {
	global $wpdb;
	parse_str($_SERVER['QUERY_STRING'],$queryStrings);

	function exists($variable){
		if($variable){
			return $variable;
		} else {
			return null;
		}
	}

	function convertDatetime($s){	
		$date = strtotime($s);
		return date('Y-m-d H:i:s', $date);			
	}

	function get_distance_between_zips($zip_from, $zip_to)
	{

			$theta = $zip_from->longitude - $zip_to->longitude;
			$dist = sin(deg2rad($zip_from->latitude)) * sin(deg2rad($zip_to->latitude)) +  cos(deg2rad($zip_from->latitude)) * cos(deg2rad($zip_to->latitude)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;	

			$distance = round($miles,2);		

		return $distance;
	}	
	
	//form an array with all the non-empty values
	$insertArray = array();
	$typeArray = array();
	foreach($queryStrings as $key => $value){
		if(isset($value) && $key != 'action'){
			//handle special keys
			if($key == 'move_date'){
				$value = convertDatetime($value);
			} 
			
			$insertArray[$key] = trim($value);
			
			if($key == 'interstate' || $key == 'international' || $key == 'auto_running'){ //need to save as integer
				$typeArray[] = '%d';
			} else {
				$typeArray[] = '%s';
			}			
		}
	}
	
	// //additional processing if certain parameters met. 
	// //if postal_code2 exists, but NOT move_from_type, then user just completed moving from, and we should run calc to get move distance
	// if( $queryStrings['postal_code2'] != "" && $queryStrings['move_from_type'] == "") {
		// $zips = $wpdb->get_results( 'SELECT * FROM wp_zipcodes WHERE zip = "' . $queryStrings['postal_code'] . '" Limit 1' );
		// foreach($zips as $zip) $zip_from = $zip;	
		// $zips = $wpdb->get_results( 'SELECT * FROM wp_zipcodes WHERE zip = "' . $queryStrings['postal_code2'] . '" Limit 1' );
		// foreach($zips as $zip) $zip_to = $zip;	
		
		// //if we have data on both zipcodes, then process to get the distance
		// if(!empty($zip_from) && !empty($zip_to) ){
			// $distance = get_distance_between_zips($zip_from, $zip_to);
			// //if we've got valid distance, then insert it into the save array, plus the datatype as integer
			// if( !empty($distance) ){
				// $insertArray["distance"] = trim($distance);
				// $typeArray[] = '%d';
			// }
		// }
		
	// }
	

	//save stuff
	$wpdb->replace(
		'wp_mpf_leads',
		$insertArray,
		$typeArray		
	);	
	//echo $wpdb->last_query;
	
	//Submission required elements
	$required_submit = array(
		'postal_code',
		'postal_code2',
		'move_date',
		'move_size',
		'email',
		'phone_number',
		'first_name',
		'last_name'
	);
	
	$submit_state = true;
	foreach($required_submit as $check){
		if( empty( $insertArray[$check] ) ) {
			$submit_state = false;
		}
	}
	if( $submit_state ) {
		$brain_response = submit_to_brain($insertArray);
		// var_dump($brain_response);
		// var_dump(json_encode($brain_response['submit']->movingcom->error_object));
		echo json_encode($brain_response['submit']->movingcom->error_object);
		
		
		//check if valid - and save to local db
		if($brain_response['auth']['success'] == false){
			//echo 'Sorry, There was an error. Please try again later';
			//send admin email - this shouldn't be happening
		}		
		
		//check if valid - and save to local db
		if($brain_response['submit']->success == true){
			$insertArray['valid'] = 1;
			$typeArray[] = '%d';
			//echo $brain_response['submit']->message;
				//save valid
				$wpdb->replace(
					'wp_mpf_leads',
					$insertArray,
					$typeArray		
				);
		} else {
			//save not valid
			$insertArray['valid'] = 0;
			$typeArray[] = '%d';
			//echo $brain_response['submit']->message;
				//save valid
				$wpdb->replace(
					'wp_mpf_leads',
					$insertArray,
					$typeArray		
				);
		}

	} 

	
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_save_mpf_steps', 'save_mpf_steps' );
add_action( 'wp_ajax_nopriv_save_mpf_steps', 'save_mpf_steps' );


function submit_to_brain($insertArray){
	global $mpf_settings;
	$insertArray['ip'] = $_SERVER['REMOTE_ADDR'];
	
	$fields = array(
		'username' => $mpf_settings['brain_api_username'],
		'password' => $mpf_settings['brain_api_password'],
	);
	
	$result = request_token($mpf_settings['api_token_url'], $fields);
	
	$response = array();
	if(isset($result->success) && $result->success === true) {
		$token = $result->data->token; //get the token.
		$response['auth']['success'] = 1;
	} else {
		$response['auth']['success'] = 0;
		$response['auth']['message'] = $result->message;
		
		return $response; //error message
		die();
		//sendErrorMessage('Authorization Attempt Failed');
	}
	$response['submit'] = submit_quote($mpf_settings['api_submit_quote_url'], $insertArray, $token);	
	//var_dump($response);
	return $response;

}

function request_token($url, $fields){
	//url-ify the data for the POST
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');
	
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60);
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);        
	$result = curl_exec($ch);
	curl_close($ch);	
	
	return json_decode($result);
}

function submit_quote($url, $insertArray, $token) {
	$ch = curl_init();                
	$authorization = "Authorization: Token ".$token;
	
	curl_setopt($ch,CURLOPT_HTTPHEADER, array(
		$authorization, 
		'Content-Type: application/json', 
		'Content-Length: ' . strlen(json_encode($insertArray))
	));
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch,CURLOPT_POST, count($insertArray));
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60);
	curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($insertArray));
	curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	
	return json_decode($result);	
}

function looking_for_movers(){
	global $wpdb;
	$count_looking_for_movers = $wpdb->get_results( 'SELECT COUNT(id) FROM wp_mpf_leads WHERE date > NOW() - INTERVAL 1 DAY' );
	foreach($count_looking_for_movers[0] as $key => $val){
		echo json_encode($val);
		break; //only want first anyways
	}
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_looking_for_movers', 'looking_for_movers' );
add_action( 'wp_ajax_nopriv_looking_for_movers', 'looking_for_movers' );

function recent_moving_distance(){
	global $wpdb;
	
	$count_looking_for_movers = $wpdb->get_results( 'SELECT SUM(distance) FROM wp_mpf_leads WHERE date > NOW() - INTERVAL 365 DAY' );
	foreach($count_looking_for_movers[0] as $key => $val){
		$miles = $val;
		break; //only want first anyways
	}
	
	$count_looking_for_movers = $wpdb->get_results( 'SELECT COUNT(id) FROM wp_mpf_leads WHERE date > NOW() - INTERVAL 365 DAY AND postal_code2 != ""' );
	foreach($count_looking_for_movers[0] as $key => $val){
		$users = $val;
		break; //only want first anyways
	}	
	
	$recent_moving_distance = array("users" => $users, "miles" => $miles);
	echo json_encode($recent_moving_distance);
	
	
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_recent_moving_distance', 'recent_moving_distance' );
add_action( 'wp_ajax_nopriv_recent_moving_distance', 'recent_moving_distance' );


function weekly_demand(){
	global $wpdb;
	parse_str($_SERVER['QUERY_STRING'],$queryStrings);
	$move_date = $queryStrings['move_date'];
	$move_date = strtotime($move_date);
	$move_date = date('Y-m-d H:i:s', $move_date);
	$weekly_demand = $wpdb->get_results( 'SELECT COUNT(id) FROM wp_mpf_leads WHERE move_date > "' . $move_date . '" - INTERVAL 7 DAY AND move_date < "' . $move_date . '" + INTERVAL 7 DAY' );
	foreach($weekly_demand[0] as $key => $val){
		echo json_encode($val);
		break; //only want first anyways
	}
	
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_weekly_demand', 'weekly_demand' );
add_action( 'wp_ajax_nopriv_weekly_demand', 'weekly_demand' );

function recent_moves() {
	global $wpdb;
	$recent_moves = $wpdb->get_results( 'SELECT DISTINCT first_name, region, region2 FROM wp_mpf_leads WHERE wp_mpf_leads.interstate = 1 AND wp_mpf_leads.first_name IS NOT NULL AND wp_mpf_leads.last_name IS NOT NULL ORDER BY wp_mpf_leads.id DESC LIMIT 20' );
	shuffle($recent_moves);
	$recent_movers = array();
	$i = 0;
	foreach($recent_moves as $row){
		if($i == 5) continue;
		if(strlen($row->first_name) > 3){
			$row->first_name = ucwords(strtolower($row->first_name));
			$recent_movers[] = $row;
		}
		$i++;
	}
	echo json_encode($recent_movers);	
	die(); // this is required to return a proper result
}
add_action( 'wp_ajax_recent_moves', 'recent_moves' );
add_action( 'wp_ajax_nopriv_recent_moves', 'recent_moves' );

