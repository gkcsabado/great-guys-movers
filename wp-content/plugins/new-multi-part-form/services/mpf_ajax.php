<?php



function check_zip_callback() {

	global $wpdb;

	$city = $_REQUEST['city'];

	$state = $_REQUEST['state'];

	$zips = $wpdb->get_results( 'SELECT DISTINCT zip FROM wp_zipcodes WHERE city = "' . $city . '" AND state = "' . $state . '" Limit 1' );

	foreach($zips as $zip) echo $zip->zip;	

	die(); // this is required to return a proper result

}

add_action( 'wp_ajax_check_zip', 'check_zip_callback' );

add_action( 'wp_ajax_nopriv_check_zip', 'check_zip_callback' );



function car_models() {

	global $wpdb;

	$year = (ISSET($_REQUEST['year']) && $_REQUEST['year']!=='') ? $_REQUEST['year'] : "2013";

	

	$results = $wpdb->get_results( "SELECT DISTINCT id, make, model FROM wp_car_models WHERE wp_car_models.year='$year' ORDER BY make asc" );

	$vehicle_models = array();	

	foreach($results as $row){

		$vehicle_models[] = $row;

	} 		

	echo json_encode($vehicle_models);

	die(); // this is required to return a proper result

}

add_action( 'wp_ajax_car_models', 'car_models' );

add_action( 'wp_ajax_nopriv_car_models', 'car_models' );



function save_mpf_steps() {

	global $wpdb;

	parse_str($_SERVER['QUERY_STRING'],$queryStrings);

	function exists($variable){

		if($variable){

			return $variable;

		} else {

			return null;

		}

	}



	function convertDatetime($s){	

		$date = strtotime($s);

		return date('Y-m-d H:i:s', $date);			

	}


	//form an array with all the non-empty values

	$insertArray = array();
	$insertToBrainArray = array();

	$typeArray = array();


	// Custom

	$mpf_uid		= (!empty($_POST['mpf_uid']) ? $_POST['mpf_uid'] : '' );
	$zipcode_from  	= (!empty($_POST['zipcode_from']) ? $_POST['zipcode_from'] : '' );
	$zipcode_to 	= (!empty($_POST['zipcode_to']) ? $_POST['zipcode_to'] : '' );
	$move_date 		= (!empty($_POST['datetimepicker']) ? $_POST['datetimepicker'] : '' );
	$move_size 		= (!empty($_POST['move_size']) ? $_POST['move_size'] : '' );
	$fname 			= (!empty($_POST['fname']) ? $_POST['fname'] : '' );
	$lname 			= (!empty($_POST['lname']) ? $_POST['lname'] : '' );
	$email 			= (!empty($_POST['email']) ? $_POST['email'] : '' );
	$phone 			= (!empty($_POST['phone']) ? $_POST['phone'] : '' );
	$auto_year 		= null;
	$auto_make		= null;
	$auto_model		= null;
	$auto_running 	= null;

	if($zipcode_from === '')
		$zipcode_from = $_POST['zipcode_from_main'];

	if($move_size === 'Auto') {
		$auto_year 		= (!empty($_POST['auto_year']) ? $_POST['auto_year'] : '' );
		$auto_make		= (!empty($_POST['auto_make']) ? $_POST['auto_make'] : '' );
		$auto_model		= (!empty($_POST['auto_model']) ? $_POST['auto_model'] : '' );
		$auto_running 	= (!empty($_POST['auto_running']) ? 1 : 0 );
	}

	// $insertArray = array(
	// 	'mpf_uid' 		=> $mpf_uid,
	// 	'street_number' => '',
	// 	'route'			=> '',
	// 	'city' => '',
	// 	'region' => 'SP',
	// 	'postal_code' 	=> $zipcode_from,
	// 	'country' => 'Brazil',
	// 	'street_number2' => '',
	// 	'route2' => '',
	// 	'city2' => 'Fuxin',
	// 	'region2' => '',
	// 	'postal_code2' 	=> $zipcode_to,
	// 	'country2' => 'China',
	// 	'move_from_type' => 'house',
	// 	'move_to_type' => 'house',
	// 	'move_date'		=> convertDatetime($move_date),
	// 	'move_size'		=> $move_size,
	// 	'first_name'	=> $fname,
	// 	'last_name'		=> $lname,
	// 	'email'			=> $email,
	// 	'phone_number'	=> $phone,
	// 	'consent_agree' => '0',
	// 	'auto_make' => '',
	// 	'auto_model' => '',
	// 	'auto_running' => '1',
	// 	'page_url' => '/'
	// );

	$insertArray = array(
		'mpf_uid' 			=> $mpf_uid,
		'street_number' 	=> '',
		'route'				=> '',
		'city' 				=> '',
		'region' 			=> '',
		'postal_code' 		=> $zipcode_from,
		'country' 			=> '',
		'street_number2' 	=> '',
		'route2' 			=> '',
		'city2' 			=> 'Lorem',
		'region2' 			=> '',
		'postal_code2' 		=> $zipcode_to,
		'country2' 			=> '',
		'move_from_type' 	=> '',
		'move_to_type' 		=> '',
		'move_date'			=> convertDatetime($move_date),
		'move_size'			=> $move_size,
		'first_name'		=> $fname,
		'last_name'			=> $lname,
		'email'				=> $email,
		'phone_number'		=> $phone,
		'consent_agree' 	=> '0',
		'auto_make'			=> $auto_make,
		'auto_model'		=> $auto_model,
		'auto_running' 		=> $auto_running,
		'page_url' 			=> '/'
	);

	//save stuff

	$wpdb->replace('wp_mpf_leads',$insertArray);

	//Submission required elements

	$required_submit = array(
		'postal_code',
		'postal_code2',
		'move_date',
		'move_size',
		'email',
		'phone_number',
		'first_name',
		'last_name'
	);

	

	$submit_state = true;

	foreach($required_submit as $check){

		if( empty( $insertArray[$check] ) ) {

			$submit_state = false;

		}

	}

	if( $submit_state ) {

		$brain_response = submit_to_brain($insertArray);


		echo json_encode($brain_response['submit']->movingcom->error_object);

		

		

		//check if valid - and save to local db

		if($brain_response['auth']['success'] == false){

			//echo 'Sorry, There was an error. Please try again later';

			//send admin email - this shouldn't be happening

		}		

		

		//check if valid - and save to local db

		if($brain_response['submit']->success == true){

			$insertArray['valid'] = 1;

			//echo $brain_response['submit']->message;

				//save valid

				$wpdb->replace('wp_mpf_leads',$insertArray);

		} else {

			//save not valid

			$insertArray['valid'] = 0;

			//echo $brain_response['submit']->message;

				//save valid

				$wpdb->replace('wp_mpf_leads',$insertArray);

		}



	}
	

	die(); // this is required to return a proper result

}

add_action( 'wp_ajax_save_mpf_steps', 'save_mpf_steps' );

add_action( 'wp_ajax_nopriv_save_mpf_steps', 'save_mpf_steps' );





function submit_to_brain($insertArray){
	
	global $mpf_settings;

	$insertArray['ip'] = $_SERVER['REMOTE_ADDR'];

	

	$fields = array(

		'username' => $mpf_settings['brain_api_username'],

		'password' => $mpf_settings['brain_api_password'],

	);

	

	$result = request_token($mpf_settings['api_token_url'], $fields);

	

	$response = array();

	if(isset($result->success) && $result->success === true) {

		$token = $result->data->token; //get the token.

		$response['auth']['success'] = 1;

	} else {

		$response['auth']['success'] = 0;

		$response['auth']['message'] = $result->message;

		

		return $response; //error message

		die();

		//sendErrorMessage('Authorization Attempt Failed');

	}

	$response['submit'] = submit_quote($mpf_settings['api_submit_quote_url'], $insertArray, $token);	

	return $response;



}



function request_token($url, $fields){

	//url-ify the data for the POST

	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }

	rtrim($fields_string, '&');

	

	$ch = curl_init();

	curl_setopt($ch,CURLOPT_URL, $url);

	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch,CURLOPT_POST, count($fields));

	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60);

	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);        

	$result = curl_exec($ch);

	curl_close($ch);	

	

	return json_decode($result);

}



function submit_quote($url, $insertArray, $token) {

	$ch = curl_init();                

	$authorization = "Authorization: Token ".$token;

	curl_setopt($ch,CURLOPT_HTTPHEADER, array(

		$authorization, 

		'Content-Type: application/json', 

		'Content-Length: ' . strlen(json_encode($insertArray))

	));

	curl_setopt($ch,CURLOPT_URL, $url);

	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);

	curl_setopt($ch,CURLOPT_POST, count($insertArray));

	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,60);

	curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($insertArray));

	curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	$result = curl_exec($ch);

	// print_r($insertArray);
	// print_r(count($insertArray));
	// print_r(json_encode($insertArray));
	// print_r(strlen(json_encode($insertArray)));
	// print_r($url);
	// print_r($result);

	curl_close($ch);

	

	return json_decode($result);	

}



function looking_for_movers(){

	global $wpdb;

	$count_looking_for_movers = $wpdb->get_results( 'SELECT COUNT(id) FROM wp_mpf_leads WHERE date > NOW() - INTERVAL 1 DAY' );

	foreach($count_looking_for_movers[0] as $key => $val){

		echo json_encode($val);

		break; //only want first anyways

	}

	die(); // this is required to return a proper result

}

add_action( 'wp_ajax_looking_for_movers', 'looking_for_movers' );

add_action( 'wp_ajax_nopriv_looking_for_movers', 'looking_for_movers' );





function weekly_demand(){

	global $wpdb;

	parse_str($_SERVER['QUERY_STRING'],$queryStrings);

	$move_date = $queryStrings['move_date'];

	$move_date = strtotime($move_date);

	$move_date = date('Y-m-d H:i:s', $move_date);

	$weekly_demand = $wpdb->get_results( 'SELECT COUNT(id) FROM wp_mpf_leads WHERE move_date > "' . $move_date . '" - INTERVAL 7 DAY AND move_date < "' . $move_date . '" + INTERVAL 7 DAY' );

	foreach($weekly_demand[0] as $key => $val){

		echo json_encode($val);

		break; //only want first anyways

	}

	

	die(); // this is required to return a proper result

}

add_action( 'wp_ajax_weekly_demand', 'weekly_demand' );

add_action( 'wp_ajax_nopriv_weekly_demand', 'weekly_demand' );



function recent_moves() {

	global $wpdb;

	$recent_moves = $wpdb->get_results( 'SELECT DISTINCT first_name, region, region2 FROM wp_mpf_leads WHERE wp_mpf_leads.interstate = 1 AND wp_mpf_leads.first_name IS NOT NULL AND wp_mpf_leads.last_name IS NOT NULL ORDER BY wp_mpf_leads.id DESC LIMIT 20' );

	shuffle($recent_moves);

	$recent_movers = array();

	$i = 0;

	foreach($recent_moves as $row){

		if($i == 5) continue;

		if(strlen($row->first_name) > 3){

			$row->first_name = ucwords(strtolower($row->first_name));

			$recent_movers[] = $row;

		}

		$i++;

	}

	echo json_encode($recent_movers);	

	die(); // this is required to return a proper result

}

add_action( 'wp_ajax_recent_moves', 'recent_moves' );

add_action( 'wp_ajax_nopriv_recent_moves', 'recent_moves' );

