<?php



//preheader

function mq_quote_preheader() {

	if  ( !is_admin() ) {

		global $post, $current_user;

		if ( !empty( $post->post_content ) && strpos ( $post->post_content, "[mq_quote]" ) !== false) {

			

		}

	}

}

add_action( 'wp', 'mq_quote_preheader', 1 );



//shortcode [mq_quote]

function mq_quote_shortcode($atts) {
   

	ob_start();

	

	//begin HTML for moving quote below

	?>

		<!-- CUSTOM CODE STARTS HERE -->

	<div class="get-quote-wrapper">
		<form action="" id="form-get-quote">
			<!-- Ready to get moving?  -->
			<h4 class="quote-headline">Can you tell us where you are moving from?</h4>
			<div class="input-group input-group-lg mb-3">
				<input type="tel" id="zipcode_from_main" class="form-control zipcode_from_main" name="zipcode_from_main" placeholder="Enter your zip code" maxlength="5" data-errtext="Zip code is required.">
				<div class="input-group-append">
					<button class="btn btn-primary btn-get-quote" data-toggle="modal" data-target="#multipart-form-modal" data-initialstep="#step_zipcode_to"><i class="fa fa-arrow-right"></i></button>
				</div>
				<small class="form-error" data-field="zipcode_from_main"></small>
			</div>
		</form>
	</div>


		<!-- CUSTOM CODE ENDS HERE -->

	<?php

	//Handle html from moving quote

	$temp_content = ob_get_contents();

	ob_end_clean();

	return $temp_content;

}

add_shortcode( 'mq_quote', 'mq_quote_shortcode' );

?>