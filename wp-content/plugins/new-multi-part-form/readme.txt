=== Multi-Part Form ===
Contributors: Moonraker
Donate link: #
Tags: Multi-Part Form, Multi Part Form, Multiple Forms, Forms
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A plugin that  has a multi-part moving form to submit to API.

== Description ==

<h3>Multi-Part Form</h3>
Multi-Part Form has a multi-part moving form that can be used through a shortcode.

== Changelog ==

= 1.0.0 =
* Initial release

== Upgrade Notice ==
Multi-Part Form
