// (function($){
jQuery(function($){

	var after_resize = (function(){
		var timer = 0;
		return function(callback, ms){
		    clearTimeout (timer);
		    timer = setTimeout(callback, ms);
		};
	})();

	// Get the toc
	var toc = $("#toc-sidebar > .widgetizedArea:last-child");
	var offset_pos_toc = 0;

	// Get the offset position of the toc
	if (toc.length)
		offset_pos_toc = toc.offset().top;


	// Add the sticky class to the toc when you reach its scroll position. Remove "sticky" when you leave the scroll position
	function tocSticky() {
		if ($(window).scrollTop() > offset_pos_toc - $("#wrapper-header-scrolled").height()) {
			toc.addClass("sticky");
			$(".sticky").css('top', $("#wrapper-header-scrolled").height() + 'px' );
			
			if((toc.offset().top + toc.outerHeight(true)) >= $(".last-separator").offset().top) {
				// console.log(offset_pos_toc);
				// toc.removeClass("sticky");
				// toc.css('top', toc.offset().top + 'px' );
			} else {
				// toc.addClass("sticky");
			}

		} else {
			$(".sticky").css('top', '0px' );
			toc.removeClass("sticky");
		}
	}

	if($(window).width() <= 991) {
		$(".lwptoc").addClass("uagb-toc__collapse");
	}
	scrollInitialized();

	$(".uagb-toc__title-wrap").click(function() {

		scrollInitialized();
	});

	function scrollInitialized() {
		after_resize(function() {
			if($(window).width() <= 991) {
				toc.removeClass("sticky");
			} else {
				tocSticky();
				$(window).scroll(function() {
					tocSticky();
				});
			}
			
			if($(".lwptoc").hasClass("uagb-toc__collapse")) {
				$('.lwptoc_items').slimScroll({destroy: true});
				$(".lwptoc_items").hide();
				$(".lwptoc_toggle_label").html("☰");
				$(".lwptoc_toggle_label").click(function() {
					$(this).html("✕");
				})
			} else {
				$(".lwptoc_items").show();
				$(".lwptoc_toggle_label").html("✕");
				$('.lwptoc_items').slimScroll({
					position: 'left',
					height: '400px',
					allowPageScroll: true,
				});
				$(".lwptoc_items").show();
				$('.lwptoc_items').css('padding-left', '15px');
			}
		}, 50);
	}

	if($(window).width() <= 767) {
		ctaMaskWidth();
	}

	$(window).resize(function() {
		if($(window).width() <= 991) {
			$(".lwptoc").addClass("uagb-toc__collapse");
		} else {
			$(".lwptoc").removeClass("uagb-toc__collapse");
		}
		scrollInitialized();
		
		if($(this).width() <= 767) {
			ctaMaskWidth();
		} else {
			$('.cta-label .mask').css({'border-width': '74px 50px'});
		}
	});

	$(".uagb-toc__list li a").click(function(e) {
		e.preventDefault();
	});

	$(".arrow-scroll").click(function() {
		var nextEl = $(this).parent().next().offset().top;
		$("html, body").animate({ scrollTop: nextEl }, 400);
	});

	$(".uagb-toc__scroll-top").click(function() {
		$(this).trigger("blur");
	});

	// $( document ).delegate( '.uagb-toc__title-wrap', 'click', UAGBTableOfContents._toggleCollapse );

	function ctaMaskWidth() {
		$('.cta-label .mask').css({'border-width': '50px '+ $('.cta-label').width()/2 +'px 0'});
	}

});
// }(jQuery));