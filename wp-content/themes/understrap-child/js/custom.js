(function($){

	var zip_header = $("#wrapper-header-scrolled");

	zipHeaderSticky();
	$(window).scroll(function(){zipHeaderSticky()});

	function zipHeaderSticky() {
		var offset_content = 0;
		if ($(".page-custom #content").length)
			offset_content = $(".page-custom #content").offset().top;
		if ($(window).scrollTop() > offset_content - 0) {
			zip_header.addClass("d-block");
			zip_header.removeClass("d-none");
		} else {
			zip_header.removeClass("d-block");
			zip_header.addClass("d-none");
		}
	}

	if($(window).width() > 767) {
		$(".navbar-collapse .dropdown-toggle").removeAttr("data-toggle");
	}

	$(window).resize(function() {
		zipHeaderSticky();
		// $(window).scroll(function(){zipHeaderSticky()});

		if($(this).width() > 767) {
			$(".navbar-collapse .dropdown-toggle").removeAttr("data-toggle");
		} else {
			$(".navbar-collapse .dropdown-toggle").attr("data-toggle", "dropdown");
		}
	});

	$(".menu-header").click(function(e) {
		e.preventDefault();
	});

	$(".navbar-toggler-custom").click(function() {
		$(this).toggleClass('no-hamburger');
		if($(this).hasClass('no-hamburger')) {
			$(".line-1").css({
				'transform-style': 'preserve-3d', 
				'transition': 'transform 250ms ease 0s', 
				'-webkit-transition': 'transform 250ms ease 0s', 
				'transform': 'rotateX(0deg) rotateY(0deg) rotateZ(45deg) translateX(6px) translateY(7px) translateZ(0px)',
				'-webkit-transform': 'rotateX(0deg) rotateY(0deg) rotateZ(45deg) translateX(6px) translateY(7px) translateZ(0px)'
			});
			$(".line-2").css({
				'transform-style': 'preserve-3d', 
				'transition': 'transform 250ms ease 0s', 
				'-webkit-transition': 'transform 250ms ease 0s', 
				'transform': 'translateX(-50px) translateY(0px) translateZ(0px)',
				'-webkit-transform': 'translateX(-50px) translateY(0px) translateZ(0px)'
			});
			$(".line-3").css({
				'transform-style': 'preserve-3d', 
				'transition': 'transform 250ms ease 0s', 
				'-webkit-transition': 'transform 250ms ease 0s', 
				'transform': 'rotateX(0deg) rotateY(0deg) rotateZ(-45deg) translateX(6px) translateY(-7px) translateZ(0px)',
				'-webkit-transform': 'rotateX(0deg) rotateY(0deg) rotateZ(-45deg) translateX(6px) translateY(-7px) translateZ(0px)'
			});
		} else {
			$(".line-1").css({
				'transform-style': 'preserve-3d', 
				'transition': 'transform 250ms ease 0s', 
				'-webkit-transition': 'transform 250ms ease 0s', 
				'transform': 'rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)',
				'-webkit-transform': 'rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)'
			});
			$(".line-2").css({
				'transform-style': 'preserve-3d', 
				'transition': 'transform 250ms ease 0s', 
				'-webkit-transition': 'transform 250ms ease 0s', 
				'transform': 'translateX(0px) translateY(0px) translateZ(0px)',
				'-webkit-transform': 'translateX(0px) translateY(0px) translateZ(0px)'
			});
			$(".line-3").css({
				'transform-style': 'preserve-3d', 
				'transition': 'transform 250ms ease 0s', 
				'-webkit-transition': 'transform 250ms ease 0s', 
				'transform': 'rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)',
				'-webkit-transform': 'rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)'
			});
		}
	});

	var btn = $('#back_to_top_button');

	$(window).scroll(function() {
		if ($(window).scrollTop() > 400) {
			btn.addClass('show');
		} else {
			btn.removeClass('show');
		}
	});

	btn.on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop:0}, '300');
	});

}(jQuery));