<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();
    wp_enqueue_style( 'child-cta-styles', get_stylesheet_directory_uri() . '/css/cta-zip.min.css', array(), false );
    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );

    // wp_enqueue_script( array('jquery') );
    wp_enqueue_script( 'child-jquery', get_stylesheet_directory_uri() . '/js/jquery.min.js', array(), false, false );
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );

    if ( is_page_template( 'page-templates/page-moving_guide.php' ) || is_page_template( 'page-templates/page-moving_guide_toc.php' ) ) {
        wp_enqueue_script( 'child-slimscroll-scripts', get_stylesheet_directory_uri() . '/js/jquery.slimscroll.js', array(), false, false);
        wp_enqueue_script( 'child-moving-guides-scripts', get_stylesheet_directory_uri() . '/js/components/moving-guides.js', array(), false, true);
    }

    wp_enqueue_script( 'child-custom-scripts', get_stylesheet_directory_uri() . '/js/custom.js', array(), false, true);

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}

// function wpse196289_default_page_template() {
//     global $post;
//     if ( 'moving_guides' == $post->post_type ) {
//         $post->page_template = 'page-templates/page-moving_guide.php';
//     }
// }
// add_action('add_meta_boxes', 'wpse196289_default_page_template', 1);

if ( function_exists('register_sidebar') )
  register_sidebar(array(
    'name' => 'TOC Sidebar',
    'id'  => 'toc-sidebar',
    'before_widget' => '<div class = "widgetizedArea">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);


function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );

/* REMOVE TITLES ON MENU */
function my_menu_notitle( $menu ){
  return $menu = preg_replace('/ title=\"(.*?)\"/', '', $menu );

}
add_filter( 'wp_nav_menu', 'my_menu_notitle' );

/********************************************************/
// Adding Dashicons in WordPress Front-end
/********************************************************/
add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}

// hide luckywp table of contents update
function filter_plugin_updates( $value ) {
    unset( $value->response['luckywp-table-of-contents/luckywp-table-of-contents.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

function add_custom_nav_item($items, $args) {
  if ($args->theme_location == 'primary') {
    $newitems = '<li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-hiw" class="with-toggle menu-header menu-how-it-works menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-hiw nav-item">
                    <a href="#" data-toggle="dropdown" aria-haspopup="true" class="dropdown-toggle nav-link"><i class="fa fa-chevron-down" aria-hidden="true"></i> How It Works</a>
                    <ul class="dropdown-menu" aria-labelledby="menu-item-dropdown-hiw" role="menu">
                        <li itemscope="itemscope" itemtype="https://www.schema.org/SiteNavigationElement" id="menu-item-9999" class="menu-item menu-item-type-post_type menu-item-object-post menu-item-9999 nav-item">
                            <div class="container">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="circle-stack fa-3x">
                                            <i class="fa fa-circle-thin"></i>
                                            <span class="circle-step">1</span>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="hiw-title">Tell us about your project</div>
                                        <div class="hiw-text">Answer a few easy questions to get matched with the best local pros.</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="circle-stack fa-3x">
                                            <i class="fa fa-circle-thin"></i>
                                            <span class="circle-step">2</span>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="hiw-title">Compare prices and options</div>
                                        <div class="hiw-text">Compare multiple bids, promotions, and contractor reviews.</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="circle-stack fa-3x">
                                            <i class="fa fa-circle-thin"></i>
                                            <span class="circle-step">3</span>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="hiw-title">Get the job done</div>
                                        <div class="hiw-text">Use our checklists, price calculators, and other online resources to make the most informed decisions.</div>
                                    </div>
                                    <div class="col-12 mt-2">
                                        <button class="btn btn-outline-primary btn-block btn-md hiw-button text-uppercase">Get Started</button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>' . $items;
  }
  return $newitems;
}
add_filter( 'wp_nav_menu_items', 'add_custom_nav_item', 10, 2 );

// wp_nav_menu();
