<?php
/**
 * Template Name: Moving Guides Page with TOC
 * Template Post Type: page, moving_guides
 * Template for displaying a page with sidebar.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_template_part( 'page-templates/hero' ); ?>

<div class="wrapper page-movingguides page-custom" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12"><?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb( '<p id="breadcrumbs" class="mb-0 mt-2">','</p>' ); } ?></div>
			
			<?php get_template_part( 'sidebar-templates/sidebar', 'toc' ); ?>

			<div
				class="<?php if ( is_active_sidebar( 'toc-sidebar' ) ) : ?>col-lg-8<?php else : ?>col-lg-12<?php endif; ?> content-area"
				id="primary">

				<main class="site-main" id="main" role="main">

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'guide' ); ?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>