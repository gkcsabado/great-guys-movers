<?php
/**
 * Hero setup.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<?php if ( has_post_thumbnail() ) { ?>
<div class="wrapper" id="wrapper-hero" style="background-image: url(<?php echo get_the_post_thumbnail_url($post->ID); ?>);">
<?php } else { ?>
<div class="wrapper" id="wrapper-hero" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/defaultbg.jpg");">
<?php } ?>

	<?php get_template_part( 'sidebar-templates/sidebar', 'customhero' ); ?>

</div>
