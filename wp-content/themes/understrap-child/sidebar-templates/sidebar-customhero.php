<?php
/**
 * Static hero sidebar setup.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$container = get_theme_mod( 'understrap_container_type' );
?>

<!-- ******************* The Hero Widget Area ******************* -->
<div class="hero-overlay"></div>
<div class="hero-content">
	<div class="container">
		<?php the_title( '<h1 class="entry-title pt-5">', '</h1>' ); ?>

		<?php echo do_shortcode('[mq_quote]'); ?>
	</div>
</div>


<div class="arrow-scroll no-bg position-absolute rounded-circle mb-3 p-3">
	<i class="fa fa-chevron-down"></i>
</div>
